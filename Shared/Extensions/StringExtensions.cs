using System.Linq;

namespace Shared.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmpty(this string s)
        {
            if (s == null)
                return true;
            if (!s.Any())
                return true;

            return false;
        }
    }
}