﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL
{
    public sealed class ApplicationContextFactory : IDesignTimeDbContextFactory<BotContext>
    {
        public ApplicationContextFactory()
        {
        }
        public BotContext CreateDbContext(string[] args)
        {
            var connection = "Server=localhost;Port=5432;Database=ResaurantDb;User Id=postgres;Password=kasymov2002";
            var options = new DbContextOptionsBuilder<BotContext>()
                .UseNpgsql(connection)
                .Options;

            return new BotContext(options);
        }
    }
}
