﻿using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Repositories
{
    public sealed class RegularQuestionRepository : DbRepository<RegularQuestion>
    {
        public RegularQuestionRepository(BotContext botContext) : base(botContext) { }
    }
}
