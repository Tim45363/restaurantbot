﻿using DLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Entities
{
    public sealed class RestaurantRepository : DbRepository<Restaurant>
    {
        public RestaurantRepository(BotContext botContext) : base(botContext) { }
    }
}
