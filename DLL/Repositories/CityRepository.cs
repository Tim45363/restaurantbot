﻿using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Repositories
{
    public sealed class CityRepository : DbRepository<City>
    {
        public CityRepository(BotContext botContext) : base(botContext) { }
    }
}
