﻿using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Repositories
{
    public sealed class CountryRepository : DbRepository<Country>
    {
        public CountryRepository(BotContext botContext) : base(botContext) { }
    }
}
