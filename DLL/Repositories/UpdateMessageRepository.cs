﻿using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Repositories
{
    public class UpdateMessageRepository : DbRepository<UpdateMessage>
    {
        public UpdateMessageRepository(BotContext botContext) : base(botContext) { }

    }
}
