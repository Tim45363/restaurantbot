﻿using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Repositories
{
    public sealed class ManagerRepository : DbRepository<Manager>
    {
        public ManagerRepository(BotContext botContext) : base(botContext) { }
    }
}
