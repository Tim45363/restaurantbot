﻿using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Repositories
{
    public sealed class CurrentDialogRepository : DbRepository<CurrentDialog>
    {
        public CurrentDialogRepository(BotContext botContext) : base(botContext) { }
    }
}

