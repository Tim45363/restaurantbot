﻿using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Repositories
{
    public sealed class TableRepository : DbRepository<Table>
    {
        public TableRepository(BotContext botContext) : base(botContext) { }
    }
}
