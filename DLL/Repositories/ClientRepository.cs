﻿using DLL.Abstractions;
using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Repositories
{
    public sealed class ClientRepository : DbRepository<Client>
    {
         public ClientRepository(BotContext botContext) : base(botContext) { }
        
    }
}
