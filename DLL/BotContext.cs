﻿using DLL.Entities;
using Microsoft.EntityFrameworkCore;

namespace DLL
{
    public class BotContext : DbContext
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Hall> Halls { get; set; }
        public DbSet<Manager> Managers { get; set; }
        public DbSet<UpdateMessage> UpdateMessages { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<RegularQuestion> RegularQuestions { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
        public DbSet<Table> Tables { get; set; }
        public DbSet<CurrentDialog> CurrentDialogs { get; set; }
        public BotContext(DbContextOptions<BotContext> options) : base(options) { }
    }
}
