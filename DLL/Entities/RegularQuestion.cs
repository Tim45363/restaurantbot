﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Entities
{
    public class RegularQuestion : BaseEntity
    {
        public string Name { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string AdditionalInfo { get; set; }
    }
}
