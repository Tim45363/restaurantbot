﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Entities
{
    public class UpdateMessage : BaseEntity
    {
        public int MessageId { get; set; }
        public int  ChatId { get; set; }
    }
}
