﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Entities
{
    public class Restaurant : BaseEntity
    {
        public string Name { get; set; }
        public City City { get; set; }
        public List<Table> Tables { get; set; }
        public List<Manager> Managers { get; set; }
        public List<Reservation> Reservations { get; set; }
        // посмотрел во сколько рестораны открываются и закрываются, везде целые часы, без минут
        public int OpeningHour { get; set; } = 8;
        public int ClosingHour { get; set; } = 23;

    }
}
