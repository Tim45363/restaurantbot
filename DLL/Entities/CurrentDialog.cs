﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Entities
{
    public class CurrentDialog : BaseEntity
    {
        public int DialogTemplateId { get; set; }
        public int AlgorithmId { get; set; }
        public int AlgorithmBranchId { get; set; }
        public Client Client { get; set; }
    }
}
