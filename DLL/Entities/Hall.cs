﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Entities
{
    public class Hall : BaseEntity
    {
        public Restaurant Restourant { get; set; }
        public List<Table> Tables { get; set; }
}
}
