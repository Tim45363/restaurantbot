﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Entities
{
    public class Reservation : BaseEntity
    {
        public DateTime Time { get; set; }
        public Client Client { get; set; }
        public Table Table { get; set; }
        public int PeopleCount { get; set; }
        public Restaurant Restaurant { get; set; }
        public int DurationInMinutes { get; set; }
        public bool Confirmed { get; set; }

    }
}
