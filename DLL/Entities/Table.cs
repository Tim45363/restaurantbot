﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLL.Entities
{
    public class Table : BaseEntity
    {
        public Hall Hall { get; set; }
        public int Number { get; set; }
        public int PlaceCount { get; set; } = 2;
    }
}
