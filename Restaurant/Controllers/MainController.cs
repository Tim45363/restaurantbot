﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using BLL.TelegramUpdateHandler.Abstractions;
using Telegram.Bot.Types;

namespace RestaurantBot.Controllers
{
    [ApiController]
    [Route("api")]
    public class MainController : ControllerBase
    {
        
        private readonly ITelegramUpdateHandler _telegramUpdateHandler;
        private readonly ILogger<MainController> _logger;
        
        public MainController(ITelegramUpdateHandler telegramUpdateHandler, ILogger<MainController> logger
            )
        {
            _telegramUpdateHandler = telegramUpdateHandler;
            _logger = logger;
        }

        [HttpPost("messages")]
        public async Task<IActionResult> Post([FromBody] Update update)
        {
            // TODO: Тут бы побольше информации, чтобы потом легче было разобраться,
            // по хорошему бы в JSON это конвертировать и сохранять в базу
            _logger.LogInformation($"Принят запрос из телеграма от {update.Message.From.FirstName + update.Message.From.LastName}");
            try
            {
                await _telegramUpdateHandler.Handle(update);
                return Ok();
            }
            catch (Exception e)
            {
                // TODO: То же самое, что и вверху
                _logger.LogCritical("При попытке обработки запроса из телеграм произошла ошибка", e);
                return BadRequest();
            }
        }
    }
}
