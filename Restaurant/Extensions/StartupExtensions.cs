using BLL;
using BLL.Abstractions;
using BLL.BotDialogProcessing;
using BLL.BotDialogProcessing.Abstractions;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_0;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_1;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_2;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_3;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_4;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_5;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_6;
using BLL.BotMessageSender;
using BLL.BotMessageSender.Abstractions;
using BLL.ReservationManager;
using BLL.ReservationManager.Abstractions;
using BLL.Services;
using BLL.TelegramUpdateHandler;
using BLL.TelegramUpdateHandler.Abstractions;
using DLL.Abstractions;
using DLL.Entities;
using DLL.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace RestaurantBot.Extensions
{
    public static class StartupExtensions
    {
        public static void AddScopedServices(this IServiceCollection services)
        {
            services.AddScoped<IDbService<City>, CityDbService>();
            services.AddScoped<IDbService<Client>, ClientDbService>();
            services.AddScoped<IDbService<Country>, CountryDbService>();
            services.AddScoped<IDbService<Hall>, HallDbService>();
            services.AddScoped<IDbService<Manager>, ManagerDbService>();
            services.AddScoped<IDbService<RegularQuestion>, RegularQuestionDbService>();
            services.AddScoped<IDbService<Reservation>, ReservationDbService>();
            services.AddScoped<IDbService<UpdateMessage>, UpdateMessageDbService>();
            services.AddScoped<IDbService<Table>, TableDbService>();
            services.AddScoped<IDbService<DLL.Entities.Restaurant>, RestaurantDbService>();

            services.AddScoped<IDbService<CurrentDialog>, CurrentDialogDbService>();
        }
        
        public static void AddScopedRepositories(this IServiceCollection services)
        {
            services.AddScoped<IDbRepository<City>, CityRepository>();
            services.AddScoped<IDbRepository<Client>, ClientRepository>();
            services.AddScoped<IDbRepository<Country>, CountryRepository>();
            services.AddScoped<IDbRepository<Hall>, HallRepository>();
            services.AddScoped<IDbRepository<UpdateMessage>, UpdateMessageRepository>();
            services.AddScoped<IDbRepository<Manager>, ManagerRepository>();
            services.AddScoped<IDbRepository<RegularQuestion>, RegularQuestionRepository>();
            services.AddScoped<IDbRepository<Reservation>, ReservationRepository>();
            services.AddScoped<IDbRepository<Table>, TableRepository>();
            services.AddScoped<IDbRepository<DLL.Entities.Restaurant>, RestaurantRepository>();

            services.AddScoped<IDbRepository<CurrentDialog>, CurrentDialogRepository>();
        }
        
        public static void AddScopedOther(this IServiceCollection services)
        {
            services.AddScoped<IDialogManager, DialogManager>();

            services.AddScoped<ITelegramUpdateHandler, TelegramUpdateHandler>();
            services.AddScoped<IReservationHandler, ReservationHandler>();
            services.AddScoped<IBotMessageSender, BotMessageSender>();

            //services.AddScoped<IDialogAlgorithm, NameInputDialogAlgorithm>();
            //services.AddScoped<IDialogAlgorithm, AreYouNewClientDialogAlgorithm>();
            //services.AddScoped<IDialogAlgorithm, InputPhoneNumberDialogAlgorithm>();
            //services.AddScoped<IDialogAlgorithm, WhereDoYouLiveDialogAlgorithm>();
            //services.AddScoped<IDialogAlgorithm, IsYourCurrentCityChangedDialogAlgorithm>();
            //services.AddScoped<IDialogAlgorithm, SpecialOpportunityDialogAlgorithm>();
            //services.AddScoped<IDialogAlgorithm, ApplySpecialDiscountDialogAlgorithm>();
            //services.AddScoped<IDialogAlgorithm, InputYourCurrentCityDialogAlgorithm>();
            //services.AddScoped<IDialogAlgorithm, CheckOutOtherOffersDialogAlgorithm>();
            //services.AddScoped<IDialogAlgorithm, GoodByeDialogAlgorithm>();
            //services.AddScoped<IReservationHandler, ReservationHandler>();
            //services.AddScoped<ITelegramUpdateHandler, TelegramUpdateHandler>();
            //services.AddScoped<IBotMessageSender, BotMessageSender>();
            //services.AddScoped<IDialogTemplate, GreetingDialogTemplate>();
        }
    }
}