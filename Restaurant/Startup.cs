using BLL;
using BLL.ReservationManager;
using DLL;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RestaurantBot.Extensions;

namespace RestaurantBot
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScopedServices();
            services.AddScopedRepositories();
            services.AddScopedOther();

            var connectionPostgreSQL = _configuration.GetConnectionString("PostgreSQL");
            

            services.AddDbContext<BotContext>(options =>
            {
                options.UseNpgsql(connectionPostgreSQL);
            });

            services.AddControllers().AddNewtonsoftJson();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<BotContext>())
                {
                    context.Database.Migrate();
                }
            }
            
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            HangFireService.CustomizeHangFire(_configuration.GetConnectionString("SqlServer"));
            
            TelegramBotRequisites.InitializeTelegramClient(_configuration["token"], _configuration["telegramApiUrl"]);
        }
    }
}
