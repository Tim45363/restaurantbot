﻿using DLL.Entities;
using System.Threading.Tasks;
using BLL.Abstractions;
using Telegram.Bot.Types;

namespace BLL.BotDialogProcessing.Abstractions
{
    public interface IDialogManager : IDataManager<Update, bool>
    {


        /// <summary>
        /// проверка Update на пренадлежность IDialogManager 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Task<bool> DoesItForMe(Update data);

        /// <summary>
        /// Управляет диалогом
        /// </summary>
        /// <param name="update"></param>
        /// <returns></returns>
        public Task<bool> Manage(Update update);
    }
}
