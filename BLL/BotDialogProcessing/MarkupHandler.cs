﻿using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing
{
    public static class MarkupHandler
    {
        public static ReplyKeyboardMarkup GetMarkupForTemplates(List<IDialogTemplate> dialogTemplates)
        {
            var dialogTemplateNames = dialogTemplates.Select(d => d.Command).ToList();
            var templatesName = new List<KeyboardButton>();

            dialogTemplateNames.ForEach(n => templatesName.Add(new KeyboardButton(n)));

            var keyboardMarkup = new ReplyKeyboardMarkup(templatesName, true);

            return keyboardMarkup;
        }
    }
}
