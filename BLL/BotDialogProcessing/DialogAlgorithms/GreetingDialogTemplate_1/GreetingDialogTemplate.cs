using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Abstractions;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_0;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_1;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_2;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_3;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_4;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_5;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_6;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using DLL.Entities;

namespace BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1
{
    public sealed class GreetingDialogTemplate : IDialogTemplate
    {
        private readonly IBotMessageSender _messageSender;
        private readonly IDbService<CurrentDialog> _currentDialogService;
        public int[] LastDialogStepsId { get; set;  } = { 10 };
        public int DialogTemplateId { get; } = 1;
        public string Command { get; } = "/start";
        private readonly List<IDialogAlgorithm> _dialogAlgorithms;

        public GreetingDialogTemplate(IBotMessageSender messageSender,
            IDbService<Client> clientService,
             IDbService<City> cityService,
            IDbService<Restaurant> restaurantService,
            IDbService<CurrentDialog> currentDialogService)
        {
            _currentDialogService = currentDialogService;
            _messageSender = messageSender;
            _dialogAlgorithms = new List<IDialogAlgorithm>() {
                new AreYouNewClientDialogAlgorithm(messageSender),
                new NameInputDialogAlgorithm(messageSender, clientService),
                new WhereDoYouLiveDialogAlgorithm(messageSender, clientService,  cityService, restaurantService),
                new InputPhoneNumberDialogAlgorithm(messageSender, clientService),
                new IsYourCurrentCityChangedDialogAlgorithm(messageSender, clientService),
                new SpecialOpportunityDialogAlgorithm(messageSender),
                new ApplySpecialDiscountDialogAlgorithm(messageSender),
                new InputYourCurrentCityDialogAlgorithm(messageSender, clientService, restaurantService),
                new CheckOutOtherOffersDialogAlgorithm(messageSender),
                new GoodByeDialogAlgorithm(messageSender),
            };
        }

        public int GetFirstAlgorithmOfNewBranch(int branchId)
        {
            var algorithmId = _dialogAlgorithms.Where(a => a.AlgorithmBranchId == branchId)
                .Min(a => a.AlgorithmStepId);

            return algorithmId;
        }

        public async Task<IDialogAlgorithm> GetCurrentAlgorithm(long chatId)
        {
            var currentDialog = await _currentDialogService.Get(c => c.Client.ChatId == chatId);
            var currentAlgorithm = _dialogAlgorithms
                .FirstOrDefault(d => d.AlgorithmBranchId == currentDialog.AlgorithmBranchId
                                     && d.AlgorithmStepId == currentDialog.AlgorithmId);

            return currentAlgorithm;
        }

        /// <summary>
        /// Методы сами должны знать где они находятся,
        /// надо заинжектить работу с базой и в этом сервисе сделать удобные методы типа:
        /// "ВерниТекущий", "ДайПредыдущий", "ДавайКСледующему"
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<bool> GoBackIfYouCan(long chatId)
        {

            var currentDialog = await _currentDialogService.Get(c => c.Client.ChatId == chatId);
            var previousAlgorithm = _dialogAlgorithms
                .FirstOrDefault(d => d.AlgorithmBranchId == currentDialog.AlgorithmBranchId
                                     && d.AlgorithmStepId == currentDialog.AlgorithmId - 1);

            if (previousAlgorithm == null)
                return false;
            
            var currentDialogState = new CurrentDialogState
            {
                DialogStepId = previousAlgorithm.AlgorithmStepId,
                DialogChatId = chatId
            };
            
            await SendNextQuestion(currentDialogState);
            return true;
        }

        public async Task SendNextQuestion(CurrentDialogState currentDialogState)
        {
            var currentAlgorithm = _dialogAlgorithms.FirstOrDefault(a=> a.AlgorithmStepId == currentDialogState.DialogStepId);
            var markup = await currentAlgorithm.GetKeyboardMarkup(currentDialogState);
            var messageToClient = await currentAlgorithm.GetMessageToClient(currentDialogState);

            await _messageSender.Send(currentDialogState.DialogChatId, messageToClient, markup);
        }
    }
}