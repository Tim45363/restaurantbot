﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Abstractions;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_2
{
    public class IsYourCurrentCityChangedDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 2;
        public int AlgorithmStepId { get; } = 6;

        private string _messageToClient { get; set; }  = $"Хорошо, тогда всё по старому, вы всё еще в ";// Москве?
        private readonly AnswerType _answerType = AnswerType.Text;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;
        private readonly IDbService<Client> _clientService;

        private readonly IBotMessageSender _messageSender;

        public IsYourCurrentCityChangedDialogAlgorithm(IBotMessageSender messageSender, IDbService<Client> clientService)
        {
            _clientService = clientService;
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                {"Да", 3},
                {"Нет", 4}
            };
        }

        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState dialogState)
        {
            var buttonTypeAnswer = new ButtonTypeAnswer
            {
                Answer = dialogState.ClientAnswer,
                AnswersCollection = _answerAndAlgorithmBranch
                    .Select(a => a.Key)
                    .ToArray()
            };

            if (!AnswerTypeHandling.AnswerTypeGuard.AnswerIsCorrect(_answerType, buttonTypeAnswer))
            {
                await _messageSender.Send(dialogState.DialogChatId, "Такого варианта ответа нет, ответьте нажатием на кнопку", null);
                return null;
            }

            var algorithmBranchByAnswer = _answerAndAlgorithmBranch
                .FirstOrDefault(o => o.Key == dialogState.ClientAnswer)
                .Value;

            dialogState.DialogBranchId = algorithmBranchByAnswer;

            return dialogState;
        }

        public Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState dialogState)
        {
            var keyboardButtons = new List<KeyboardButton>();
            foreach (var option in _answerAndAlgorithmBranch)
            {
                var newKeyboardButton = new KeyboardButton(option.Key);
                keyboardButtons.Add(newKeyboardButton);
            }

            var replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardButtons, true);

            return Task.FromResult<ReplyKeyboardMarkup>(replyKeyboardMarkup);
        }

        public async Task<string> GetMessageToClient(CurrentDialogState dialogState)
        {
            var client = await _clientService.Get(c => c.ChatId == dialogState.DialogChatId);
            _messageToClient += client.City.Name;

            return _messageToClient;
        }
    }
}
