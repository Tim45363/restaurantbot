﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Abstractions;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_0
{
    public class NameInputDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmStepId { get; } = 2;
        private string _messageToClient { get; set; } = "Введите ваше имя";
        public int AlgorithmBranchId { get; } = 0;
        private readonly IBotMessageSender _messageSender;
        private readonly AnswerType _answerType = AnswerType.Text;
        private readonly IDbService<Client> _clientService;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;
        public NameInputDialogAlgorithm(IBotMessageSender messageSender, IDbService<Client> clientService)
        {
            _messageSender = messageSender;
            _clientService = clientService;
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                //значит любой ответ примим 
                { string.Empty, 1} 
            };
        }


        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState dialogState)
        {
            var algorithmBranchByAnswer = _answerAndAlgorithmBranch
                .FirstOrDefault(o => o.Key == dialogState.ClientAnswer || o.Key == string.Empty)
                .Value;

            dialogState.DialogBranchId = algorithmBranchByAnswer;

            var client = await _clientService.Get(c => c.ChatId == dialogState.DialogChatId);

            if (client == null)
            {
                await _clientService.Create(new Client { Name = dialogState.ClientAnswer });
                return dialogState;
            }

            client.Name = dialogState.ClientAnswer;

            await _clientService.Update(client);

            return dialogState;
        }

        public Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState dialogState)
        {
            var keyboardButtons = new List<KeyboardButton>();
            foreach (var option in _answerAndAlgorithmBranch)
            {
                var newKeyboardButton = new KeyboardButton(option.Key);
                keyboardButtons.Add(newKeyboardButton);
            }

            var replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardButtons, true);

            return Task.FromResult<ReplyKeyboardMarkup>(replyKeyboardMarkup);
        }

        public Task<string> GetMessageToClient(CurrentDialogState dialogState)
        {
            return Task.FromResult<string>(_messageToClient);
        }
    }
}
