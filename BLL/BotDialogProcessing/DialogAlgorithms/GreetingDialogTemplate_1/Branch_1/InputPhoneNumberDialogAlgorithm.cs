﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Abstractions;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_1
{
    public class InputPhoneNumberDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 1;
        public int AlgorithmStepId { get; } = 3;

        private string _messageToClient { get; set; } = "Напишите ваш номер телефона";
        private readonly AnswerType _answerType = AnswerType.Phone;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;
        private readonly IDbService<Client> _clientService;

        private readonly IBotMessageSender _messageSender;

        public InputPhoneNumberDialogAlgorithm(IBotMessageSender messageSender, IDbService<Client> clientService)
        {
            _messageSender = messageSender;
            _clientService = clientService;
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                //значит любой ответ примим
                { string.Empty, 1}
            };
        }

        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState dialogState)
        {
            var buttonTypeAnswer = new ButtonTypeAnswer
            {
                Answer = dialogState.ClientAnswer,
                AnswersCollection = _answerAndAlgorithmBranch
                    .Select(a => a.Key)
                    .ToArray()
            };

            if (!AnswerTypeHandling.AnswerTypeGuard.AnswerIsCorrect(_answerType, buttonTypeAnswer))
            {
                await _messageSender.Send(dialogState.DialogChatId, "Телефон введён некоректо, попробуйте ещё раз", null);
                return null;
            }

            var algorithmBranchByAnswer = _answerAndAlgorithmBranch
                .FirstOrDefault(o => o.Key == dialogState.ClientAnswer || o.Key == string.Empty)
                .Value;

            dialogState.DialogBranchId = algorithmBranchByAnswer;

            var client = await _clientService.Get(c => c.ChatId == dialogState.DialogChatId);

            if(client == null)
            {
                dialogState.DialogBranchId = 0;
                await _messageSender.Send(dialogState.DialogChatId, "Ваш аккаунт не найден", null);
            }

            client.PhoneNumber = dialogState.ClientAnswer;

            await _clientService.Update(client);

            return dialogState;
        }

        public Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState dialogState)
        {
            var keyboardButtons = new List<KeyboardButton>();
            foreach (var option in _answerAndAlgorithmBranch)
            {
                var newKeyboardButton = new KeyboardButton(option.Key);
                keyboardButtons.Add(newKeyboardButton);
            }

            var replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardButtons, true);

            return Task.FromResult<ReplyKeyboardMarkup>(replyKeyboardMarkup);
        }

        public Task<string> GetMessageToClient(CurrentDialogState dialogState)
        {
            return Task.FromResult<string>(_messageToClient);
        }
    }
}
