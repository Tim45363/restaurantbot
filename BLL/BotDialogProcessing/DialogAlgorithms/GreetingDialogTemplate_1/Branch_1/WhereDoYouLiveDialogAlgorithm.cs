﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Abstractions;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_1
{
    public class WhereDoYouLiveDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 1;
        public int AlgorithmStepId { get; } = 4;

        private string _messageToClient { get; set; }  = $"Приятно познакомиться!{"\n"}В каком городе вы живете?";
        private readonly AnswerType _answerType = AnswerType.Text;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;
        private readonly IDbService<Restaurant> _restaurantService;
        private readonly IDbService<City> _cityService;
        private readonly IDbService<Client> _clientService;

        private readonly IBotMessageSender _messageSender;

        public WhereDoYouLiveDialogAlgorithm(IBotMessageSender messageSender,
            IDbService<Client> clientService,
            IDbService<City> cityService,
            IDbService<Restaurant> restaurantService)
        {
            _cityService = cityService;
            _clientService = clientService;
            _restaurantService = restaurantService;
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                //значит любой ответ примим 
                { string.Empty, 3}
            };
        }

        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState dialogState)
        {
            //var restaurants = await _restaurantService.GetMany(r => r.IsActive);
            var cities = await _cityService.GetMany(c => c.IsActive);

            var buttonTypeAnswer = new ButtonTypeAnswer
            {
                Answer = dialogState.ClientAnswer,
                AnswersCollection = cities
                    .Select(c => c.Name)
                    .ToArray()
            };

            if (!AnswerTypeHandling.AnswerTypeGuard.AnswerIsCorrect(_answerType, buttonTypeAnswer))
            {
                await _messageSender.Send(dialogState.DialogChatId, "Такого варианта ответа нет, ответьте нажатием на кнопку", null);
                return null;
            }

            var algorithmBranchByAnswer = _answerAndAlgorithmBranch
                .FirstOrDefault(o => o.Key == dialogState.ClientAnswer || o.Key == string.Empty)
                .Value;

            dialogState.DialogBranchId = algorithmBranchByAnswer;

            var client = await _clientService.Get(c => c.ChatId == dialogState.DialogChatId);

            if (client == null)
            {
                dialogState.DialogBranchId = 0;
                await _messageSender.Send(dialogState.DialogChatId, "Ваш аккаунт не найден", null);
            }
            var city = await _cityService.Get(c => c.Name == dialogState.ClientAnswer);

            client.City = city;

            await _clientService.Update(client);

            return dialogState;
        }

        public async Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState dialogState)
        {
            var cities = await _cityService.GetMany(c => c.IsActive);

            var keyboardButtons = new List<KeyboardButton>();
            foreach (var city in cities)
            {
                var newKeyboardButton = new KeyboardButton(city.Name);
                keyboardButtons.Add(newKeyboardButton);
            }

            var replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardButtons, true);

            return replyKeyboardMarkup;
        }

        public Task<string> GetMessageToClient(CurrentDialogState dialogState)
        {
            return Task.FromResult<string>(_messageToClient);
        }
    }
}
