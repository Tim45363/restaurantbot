﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_6
{
    public class GoodByeDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 6;
        public int AlgorithmStepId { get; } = 10;

        private string _messageToClient { get; set; } = $"Тогда всего хорошего!";
        private readonly AnswerType _answerType = AnswerType.Text;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;

        private readonly IBotMessageSender _messageSender;

        public GoodByeDialogAlgorithm(IBotMessageSender messageSender)
        {
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                //значит все 
            };
        }

        public Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState dialogState)
        {
            throw new Exception("Недопустимый вызов функции HandleAnswerFromClient в GoodByeDialogAlgorithm"); 
        }

        public Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState dialogState)
        {
            return null; 
        }

        public Task<string> GetMessageToClient(CurrentDialogState dialogState)
        {
            return Task.FromResult<string>(_messageToClient);
        }
    }
}
