﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Abstractions;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_4
{
    public class InputYourCurrentCityDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 4;
        public int AlgorithmStepId { get; } = 8;

        private string _messageToClient { get; set; }  = $"Введите город";
        private readonly AnswerType _answerType = AnswerType.Text;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;

        private readonly IBotMessageSender _messageSender;
        private readonly IDbService<Restaurant> _restaurantService;
        private readonly IDbService<Client> _clientService;

        public Dictionary<string, int> AnswerAndAlgorithmBranch { get; set; }

        public InputYourCurrentCityDialogAlgorithm(IBotMessageSender messageSender, IDbService<Client> clientService,
            IDbService<Restaurant> restaurantService)
        {
            _clientService = clientService;
            _restaurantService = restaurantService;
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                //значит любой ответ примим 
                { string.Empty, 3}
        };
        }

        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState dialogState)
        {
            var cities = (await _restaurantService.GetMany(r => r.IsActive))
                .Select(r => r.City.Name);

            var buttonTypeAnswer = new ButtonTypeAnswer
            {
                Answer = dialogState.ClientAnswer,
                AnswersCollection = cities.ToArray()
            };

            if (!AnswerTypeHandling.AnswerTypeGuard.AnswerIsCorrect(_answerType, buttonTypeAnswer))
            {
                await _messageSender.Send(dialogState.DialogChatId, "Такого варианта ответа нет, ответьте нажатием на кнопку", null);
                return null;
            }

            var algorithmBranchByAnswer = _answerAndAlgorithmBranch
                .FirstOrDefault(o => o.Key == dialogState.ClientAnswer || o.Key == string.Empty)
                .Value;

            dialogState.DialogBranchId = algorithmBranchByAnswer;

            var client = await _clientService.Get(c => c.ChatId == dialogState.DialogChatId);

            client.City = new City { Name = dialogState.ClientAnswer };

            await _clientService.Update(client);

            return dialogState;
        }

        public async Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState dialogState)
        {
            var cities = (await _restaurantService.GetMany(r => r.IsActive))
                .Select(r => r.City.Name);

            var keyboardButtons = new List<KeyboardButton>();
            foreach (var city in cities)
            {
                var newKeyboardButton = new KeyboardButton(city);
                keyboardButtons.Add(newKeyboardButton);
            }

            var replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardButtons, true);

            return replyKeyboardMarkup;
        }

        public Task<string> GetMessageToClient(CurrentDialogState dialogState)
        {
            return Task.FromResult<string>(_messageToClient);
        }
    }
}
