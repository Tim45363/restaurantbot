﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1.Branch_5
{
    public class CheckOutOtherOffersDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 5;
        public int AlgorithmStepId { get; } = 9;

        private string _messageToClient { get; set; } = $"Тогда можете посмотреть другие акции для вас (ссылка), может быть заинтересует?";
        private readonly AnswerType _answerType = AnswerType.Text;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;

        private readonly IBotMessageSender _messageSender;


        public CheckOutOtherOffersDialogAlgorithm(IBotMessageSender messageSender)
        {
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                {"Давайте", -1},
                {"нет, спасибо", 6}
            };
        }

        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState dialogState)
        {
            var buttonTypeAnswer = new ButtonTypeAnswer
            {
                Answer = dialogState.ClientAnswer,
                AnswersCollection = _answerAndAlgorithmBranch
                    .Select(a => a.Key)
                    .ToArray()
            };

            if (!AnswerTypeHandling.AnswerTypeGuard.AnswerIsCorrect(_answerType, buttonTypeAnswer))
            {
                await _messageSender.Send(dialogState.DialogChatId, "Такого варианта ответа нет, ответьте нажатием на кнопку", null);
                return null;
            }

            var algorithmBranchByAnswer = _answerAndAlgorithmBranch
                .FirstOrDefault(o => o.Key == dialogState.ClientAnswer)
                .Value;

            dialogState.DialogBranchId = algorithmBranchByAnswer;

            return dialogState;
        }

        public Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState dialogState)
        {
            var keyboardButtons = new List<KeyboardButton>();
            foreach (var option in _answerAndAlgorithmBranch)
            {
                var newKeyboardButton = new KeyboardButton(option.Key);
                keyboardButtons.Add(newKeyboardButton);
            }

            var replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardButtons, true);

            return Task.FromResult<ReplyKeyboardMarkup>(replyKeyboardMarkup);
        }

        public Task<string> GetMessageToClient(CurrentDialogState dialogState)
        {
            return Task.FromResult<string>(_messageToClient);
        }
    }
}
