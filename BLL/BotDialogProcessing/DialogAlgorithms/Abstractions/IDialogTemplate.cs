using System.Threading.Tasks;
using BLL.Models;

namespace BLL.BotDialogProcessing.DialogAlgorithms.Abstractions
{
    public interface IDialogTemplate
    {
        /// <summary>
        /// Возврати текущий алгоритм
        /// </summary>
        public Task<IDialogAlgorithm> GetCurrentAlgorithm(long cahtId);
        
        /// <summary>
        /// Уходим на шаг назад если можно
        /// </summary>
        public Task<bool> GoBackIfYouCan(long cahtId);

        /// <summary>
        /// Возвращает первый шаг в новой ветке
        /// </summary>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public int GetFirstAlgorithmOfNewBranch(int branchId);

        public int[] LastDialogStepsId { get; set; }

        /// <summary>
        /// Отправь следующий вопрос клиенту
        /// </summary>
        /// <returns></returns>
        public Task SendNextQuestion(CurrentDialogState currentDialogState);

        public int DialogTemplateId { get; }
        public string Command { get; }

        /// <summary>
        /// Идем на шаг вперед если можно
        /// </summary>
        /// <param name="update"></param>
        /// Под вопросом, пока не придумал как реализовать
        //public Task<IDialogAlgorithm> GetAllPreviousSteps();
    }
}