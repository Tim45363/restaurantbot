﻿using System.Threading.Tasks;
using BLL.Models;
using DLL.Entities;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.Abstractions
{
    public interface IDialogAlgorithm
    {
        int AlgorithmBranchId { get; }
        int AlgorithmStepId { get; }

        /// <summary>
        /// Обрабатывет ответ клиента
        /// </summary>
        /// <param name="currentDialogState"></param>
        Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState currentDialogState);

        Task<string> GetMessageToClient(CurrentDialogState currentDialogState);

        Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState currentDialogState);
    }
}
