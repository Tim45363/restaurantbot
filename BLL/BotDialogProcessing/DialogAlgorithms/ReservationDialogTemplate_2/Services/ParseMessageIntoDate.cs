﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Services
{
    public static class ParseMessageIntoDate
    {
        private static Dictionary<string, int> _monthToNumber { get; } = new Dictionary<string, int>
        {
            { "января", 1},
            { "феврвля", 2},
            { "марта", 3},
            { "апреля", 4},
            { "мая", 5},
            { "июня", 6},
            { "июля", 7},
            { "августа", 8},
            { "сентября", 9},
            { "октября", 10},
            { "ноября", 11},
            { "декабря", 12}
        };
        /// <summary>
        /// null если ошибка
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string TryParseDateToString(string message)
        {
            int result;
            
            if (Int32.TryParse(message.Split(" ")[0], out result))
            {
                var numberOfMonth = _monthToNumber.FirstOrDefault(m => m.Key.Contains(message.Split(" ")[1])).Value;
                if(numberOfMonth != 0 && result < 31)
                    return Parse(message).ToString();
            }

            return null;
        }

        private static DateTime Parse(string message)
        {

            var day = message.Split(" ")[0];
            var month = message.Split(" ")[1].ToLower();

            var numberOfMonth = _monthToNumber.FirstOrDefault(m => m.Key.Contains(month)).Value;

            var currentYear = DateTime.Now.Year;

            var parsedDate = DateTime.Parse($"{day}.{numberOfMonth}.{currentYear}").ToLocalTime();

            var currentMonth = DateTime.Now.Month;

            if (currentMonth > numberOfMonth)
                parsedDate = parsedDate.AddYears(1);

            return parsedDate;

        }
    }
}
