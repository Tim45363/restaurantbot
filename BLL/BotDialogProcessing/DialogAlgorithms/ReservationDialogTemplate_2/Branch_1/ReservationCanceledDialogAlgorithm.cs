﻿using BLL.Abstractions;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Branch_1
{
    public class ReservationCanceledDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 1;
        public int AlgorithmStepId { get; } = 8;

        private string _messageToClient { get; set; } = $"Бронирование отменено";

        private readonly AnswerType _answerType = AnswerType.Text;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;

        private readonly IDbService<Reservation> _reservationService;

        private readonly IBotMessageSender _messageSender;
        public ReservationCanceledDialogAlgorithm(IBotMessageSender messageSender, IDbService<Reservation> reservationService)
        {
            _messageSender = messageSender;
            _reservationService = reservationService;
        }

        public Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState currentDialogState)
        {
            throw new Exception("Недопустимый вызов функции HandleAnswerFromClient в ReservationCanceledDialogAlgorithm");
        }

        public async Task<string> GetMessageToClient(CurrentDialogState currentDialogState)
        {
            var reservation = await _reservationService.Get(r => r.Client.ChatId == currentDialogState.DialogChatId && !r.Confirmed);

            await _reservationService.Delete(reservation.Id);

            return _messageToClient;
        }

        public Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState currentDialogState)
        {
            return null;
        }
    }
}
