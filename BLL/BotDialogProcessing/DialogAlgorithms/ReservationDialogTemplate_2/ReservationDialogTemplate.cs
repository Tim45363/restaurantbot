﻿using BLL.Abstractions;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Branch_0;
using BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Branch_1;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.ReservationManager;
using BLL.ReservationManager.Abstractions;
using BLL.TelegramUpdateHandler.Abstractions;
using DLL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2
{
    public class ReservationDialogTemplate : IDialogTemplate
    {
        public int[] LastDialogStepsId { get; set;  } = { 8, 7 };
        private readonly List<IDialogAlgorithm> _dialogAlgorithms;

        public int DialogTemplateId { get; } = 2;

        public string Command { get; } = "Забронировать столик";
        private readonly IBotMessageSender _messageSender;

        private readonly IDbService<CurrentDialog> _currentDialogService;
        public ReservationDialogTemplate(IBotMessageSender messageSender,
            IDbService<Restaurant> restaurantService,
            IDbService<Client> clientService,
            IReservationHandler reservationHandler,
            IDbService<Reservation> reservationService,
            IDbService<CurrentDialog> currentDialogService)
        {
            _messageSender = messageSender;
            _currentDialogService = currentDialogService;
            _dialogAlgorithms = new List<IDialogAlgorithm>()
            {
                new ChooseRestaurantDialogAlgorithm(messageSender, restaurantService, clientService, reservationService),
                new HowManyPeopleDialogAlgorithm(messageSender, reservationService, restaurantService),
                new ConfirmReservationDialogAlgorithm(messageSender, reservationService, reservationHandler),
                new ReservationDateDialogAlgorithm(messageSender, reservationService),
                new ReservationDurationDialogAlgorithm(messageSender, reservationService),
                new ReservationTimeDialogAlgorithm(messageSender, reservationService, reservationHandler),
                new SendReservationDataDialogAlgorithm(messageSender, reservationService),
                new ReservationCanceledDialogAlgorithm(messageSender, reservationService)
            };
        }


        public async Task<IDialogAlgorithm> GetCurrentAlgorithm(long chatId)
        {
            var currentDialog = await _currentDialogService.Get(c => c.Client.ChatId == chatId);
            var currentAlgorithm = _dialogAlgorithms
                .FirstOrDefault(d => d.AlgorithmBranchId == currentDialog.AlgorithmBranchId
                                     && d.AlgorithmStepId == currentDialog.AlgorithmId);

            return currentAlgorithm;
        }

        public int GetFirstAlgorithmOfNewBranch(int branchId)
        {
            var algorithmId = _dialogAlgorithms.Where(a => a.AlgorithmBranchId == branchId)
                .Min(a => a.AlgorithmStepId);

            return algorithmId;
        }

        public async Task<bool> GoBackIfYouCan(long chatId)
        {
            var currentDialog = await _currentDialogService.Get(c => c.Client.ChatId == chatId);
            var previousAlgorithm = _dialogAlgorithms
                .FirstOrDefault(d => d.AlgorithmBranchId == currentDialog.AlgorithmBranchId
                                     && d.AlgorithmStepId == currentDialog.AlgorithmId - 1);

            if (previousAlgorithm == null)
                return false;

            var currentDialogState = new CurrentDialogState
            {
                DialogStepId = previousAlgorithm.AlgorithmStepId,
                DialogChatId = chatId
            };

            await SendNextQuestion(currentDialogState);
            return true;
        }

        public async Task SendNextQuestion(CurrentDialogState currentDialogState)
        {

            var currentAlgorithm = _dialogAlgorithms.FirstOrDefault(a => a.AlgorithmStepId == currentDialogState.DialogStepId);
            var markup = await currentAlgorithm.GetKeyboardMarkup(currentDialogState);
            var messageToClient = await currentAlgorithm.GetMessageToClient(currentDialogState);

            await _messageSender.Send(currentDialogState.DialogChatId, messageToClient, markup);
        }
    }
}
