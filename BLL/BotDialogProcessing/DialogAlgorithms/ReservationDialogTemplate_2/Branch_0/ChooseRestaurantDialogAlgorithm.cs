﻿using BLL.Abstractions;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Branch_0
{
    public class ChooseRestaurantDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 0;
        public int AlgorithmStepId { get; } = 1;

        private string _messageToClient { get; set; } = $"Выберите ресторан";

        private readonly AnswerType _answerType = AnswerType.Button;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;
        private readonly IDbService<Client> _clientService;

        private readonly IBotMessageSender _messageSender;
        private readonly IDbService<Restaurant> _restaurantService;
        private readonly IDbService<Reservation> _reservationService;
        public ChooseRestaurantDialogAlgorithm(IBotMessageSender messageSender, IDbService<Restaurant> restaurantService,
            IDbService<Client> clientService, IDbService<Reservation> reservationService)
        {
            _reservationService = reservationService;
            _clientService = clientService;
            _messageSender = messageSender;
            _restaurantService = restaurantService;
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                //значит любой ответ примим
                { string.Empty, 1}
            };
        }

        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState dialogState)
        {

            var restaurant = await _restaurantService.Get(r => r.Name == dialogState.ClientAnswer);

            if (restaurant == null)
            {
                await _messageSender.Send(dialogState.DialogChatId, "Такого варианта ответа нет, ответьте нажатием на кнопку", null);
                return null;
            }
            var client = await _clientService.Get(c => c.ChatId == dialogState.DialogChatId);

            if (client == null)
                return null;

            var newReservation = new Reservation
            {
                Client = client,
                Restaurant = restaurant
            };

            await _reservationService.Create(newReservation);

            return dialogState;
        }

        public Task<string> GetMessageToClient(CurrentDialogState currentDialogState)
        {
            return Task.FromResult<string>(_messageToClient);
        }

        public async Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState currentDialogState)
        {
            var keyboardButtons = new List<KeyboardButton>();
            foreach (var option in _answerAndAlgorithmBranch.Skip(1))
            {
                var newKeyboardButton = new KeyboardButton(option.Key);
                keyboardButtons.Add(newKeyboardButton);
            }
            var client = await _clientService.Get(c => c.ChatId == currentDialogState.DialogChatId);

            var restaurants = await _restaurantService.GetMany(r => r.IsActive);
                
            var restaurantsWithNeededCity = restaurants.Where(r => r.City.Id == client.City.Id);

            foreach (var restaurant in restaurants)
            {
                var newKeyboardButton = new KeyboardButton(restaurant.Name);
                keyboardButtons.Add(newKeyboardButton);
            }

            var replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardButtons, true);

            return replyKeyboardMarkup;
        }

    }
}
