﻿using BLL.Abstractions;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Services;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Branch_0
{
    public class ReservationDateDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 0;
        public int AlgorithmStepId { get; } = 4;

        private string _messageToClient { get; set; } = $"На какое число забронировать столик?";

        private readonly AnswerType _answerType = AnswerType.Date;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;
        private readonly IDbService<Reservation> _reservationService;


        private readonly IBotMessageSender _messageSender;
        public ReservationDateDialogAlgorithm(IBotMessageSender messageSender, IDbService<Reservation> reservationService)
        {
            _messageSender = messageSender;
            _reservationService = reservationService;
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                //значит любой ответ примим
                { string.Empty, 1}
            };
        }

        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState dialogState)
        {
            var buttonTypeAnswer = new ButtonTypeAnswer
            {
                Answer = dialogState.ClientAnswer,
                AnswersCollection = _answerAndAlgorithmBranch
                 .Select(a => a.Key)
                 .ToArray()
            };

            if (!AnswerTypeHandling.AnswerTypeGuard.AnswerIsCorrect(_answerType, buttonTypeAnswer))
            {
                await _messageSender.Send(dialogState.DialogChatId, "Такого варианта ответа нет, ответьте нажатием на кнопку", null);
                return null;
            }

            var parsedDate = ParseMessageIntoDate.TryParseDateToString(dialogState.ClientAnswer);

            var algorithmBranchByAnswer = _answerAndAlgorithmBranch
                .FirstOrDefault(o => o.Key == dialogState.ClientAnswer || o.Key == string.Empty)
                .Value;

            dialogState.DialogBranchId = algorithmBranchByAnswer;

            var reservation = await _reservationService.Get(r => r.Client.ChatId == dialogState.DialogChatId && !r.Confirmed);

            if (reservation == null)
                return null;

            reservation.Time = DateTime.Parse(parsedDate);

            await _reservationService.Update(reservation);

            return dialogState;
        }

        public Task<string> GetMessageToClient(CurrentDialogState currentDialogState)
        {
            return Task.FromResult<string>(_messageToClient);
        }

        public Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState currentDialogState)
        {
            return null;
        }
    }
}
