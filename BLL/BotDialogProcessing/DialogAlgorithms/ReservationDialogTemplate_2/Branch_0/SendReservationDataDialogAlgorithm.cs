﻿using BLL.Abstractions;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Branch_0
{
    public class SendReservationDataDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 0;
        public int AlgorithmStepId { get; } = 7;
        private readonly IDbService<Reservation> _reservationService;
        private string _messageToClient { get; set; } = $"Столик забронирован в ресторане ";//*ресторан* на *время*

        private readonly AnswerType _answerType = AnswerType.Button;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;

        private readonly IBotMessageSender _messageSender;
        public SendReservationDataDialogAlgorithm(IBotMessageSender messageSender, IDbService<Reservation> reservationService)
        {
            _reservationService = reservationService;
            _messageSender = messageSender;
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                //значит все 
            };
        }

        public Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState currentDialogState)
        {
            throw new Exception("Недопустимый вызов функции HandleAnswerFromClient в SendReservationDataDialogAlgorithm");
        }

        public async Task<string> GetMessageToClient(CurrentDialogState currentDialogState)
        {
            var reservations = await _reservationService.GetMany(r => r.Client.ChatId == currentDialogState.DialogChatId);

            var reservation = reservations.FirstOrDefault(rest => rest.CreationDate == reservations.Max(r => r.CreationDate));

            var textToAdd = $@"{reservation.Restaurant.Name} на {reservation.Time.Date} число в {reservation.Time.Hour}.{reservation.Time.Minute}";

            _messageToClient += textToAdd;

            return _messageToClient;
        }

        public Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState currentDialogState)
        {
            return null;
        }

    }
}
