﻿using BLL.Abstractions;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Branch_0
{
    public class ReservationDurationDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 0;
        public int AlgorithmStepId { get; } = 3;

        private string _messageToClient { get; set; } = $"Сколько времени вы планируете провести в ресторане?";

        private readonly AnswerType _answerType = AnswerType.Date;
        private readonly IDbService<Reservation> _reservationService;
        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;

        private readonly IBotMessageSender _messageSender;
        public ReservationDurationDialogAlgorithm(IBotMessageSender messageSender, IDbService<Reservation> reservationService)
        {
            _reservationService = reservationService;
            _messageSender = messageSender;
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                //значит любой ответ примим
                { string.Empty, 1}
            };
        }

        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState currentDialogState)
        {
            int duration = 0;
            switch (currentDialogState.ClientAnswer)
            {
                case "1 час":
                    duration = 60;
                    break;
                case "40 мин":
                    duration = 40;
                    break;
                case "30 мин":
                    duration = 30;
                    break;
                case "20 мин":
                    duration = 20;
                    break;
                case "15 мин":
                    duration = 15;
                    break;
                default:
                    await _messageSender.Send(currentDialogState.DialogChatId, "Такого варианта ответа нет, ответьте нажатием на кнопку", null);
                    return null;
            }

            var reservation = await _reservationService.Get(r => r.Client.ChatId == currentDialogState.DialogChatId
             && !r.Confirmed);

            reservation.DurationInMinutes = duration;

            await _reservationService.Update(reservation);

            return currentDialogState;
        }

        public  Task<string> GetMessageToClient(CurrentDialogState currentDialogState)
        {
            return Task.FromResult<string>(_messageToClient);
        }

        public Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState currentDialogState)
        {

            var keyboardButtons = new List<List<KeyboardButton>>() 
            { 
                new List<KeyboardButton>(){ new KeyboardButton("1 час"), new KeyboardButton("40 мин"), new KeyboardButton("30 мин") },
                new List<KeyboardButton>(){ new KeyboardButton("20 мин"), new KeyboardButton("15 мин")}
            };
            var replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardButtons, true);

            return Task.FromResult<ReplyKeyboardMarkup>(replyKeyboardMarkup);
        }
    }
}
