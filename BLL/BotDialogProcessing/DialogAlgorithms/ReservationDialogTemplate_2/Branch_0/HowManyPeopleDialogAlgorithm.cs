﻿using BLL.Abstractions;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Branch_0
{
    public class HowManyPeopleDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 0;
        public int AlgorithmStepId { get; } = 2;
        private readonly IDbService<Reservation> _reservationService;
        private readonly IBotMessageSender _messageSender;
        private readonly IDbService<Restaurant> _restaurantService;
        private string _messageToClient { get; set; } = $"На сколько человек забронировать столик?";

        private readonly AnswerType _answerType = AnswerType.Button;
        public HowManyPeopleDialogAlgorithm(IBotMessageSender messageSender, IDbService<Reservation> reservationService,
           IDbService<Restaurant> restaurantService )
        {
            _reservationService = reservationService;
            _messageSender = messageSender;
            _restaurantService = restaurantService;
        }
        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState dialogState)
        {

            var reservation = await _reservationService.Get(r => r.Client.ChatId == dialogState.DialogChatId
             && !r.Confirmed);

            var tableSeats = new List<string>();

            foreach (var table in reservation.Restaurant.Tables)
            {
                if (tableSeats.Contains(table.PlaceCount.ToString()))
                    continue;

                tableSeats.Add(table.PlaceCount.ToString());
            }

            var buttonTypeAnswer = new ButtonTypeAnswer
            {
                Answer = dialogState.ClientAnswer,
                AnswersCollection = tableSeats.ToArray()
            };

            if (!AnswerTypeHandling.AnswerTypeGuard.AnswerIsCorrect(_answerType, buttonTypeAnswer))
            {
                await _messageSender.Send(dialogState.DialogChatId, "Такого варианта ответа нет, ответьте нажатием на кнопку", null);
                return null;
            }

            reservation.PeopleCount = Int32.Parse(dialogState.ClientAnswer);

            await _reservationService.Update(reservation);

            return dialogState;
        }

        public Task<string> GetMessageToClient(CurrentDialogState currentDialogState)
        {
            return Task.FromResult<string>(_messageToClient);
        }

        public async Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState currentDialogState)
        {
            var reservation = await _reservationService.Get(r => r.Client.ChatId == currentDialogState.DialogChatId && !r.Confirmed);
            var restaurant = await _restaurantService.Get(r => r.Id == reservation.Restaurant.Id);
            var keyboardButtons = new List<KeyboardButton>();
            var tableSeats = new List<int>();

            foreach(var table in reservation.Restaurant.Tables)
            {
                if (tableSeats.Contains(table.PlaceCount))
                    continue;
                tableSeats.Add(table.PlaceCount);
                keyboardButtons.Add(new KeyboardButton(table.PlaceCount.ToString()));
            }

            var replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardButtons, true);
            return replyKeyboardMarkup;
        }
    }
}
