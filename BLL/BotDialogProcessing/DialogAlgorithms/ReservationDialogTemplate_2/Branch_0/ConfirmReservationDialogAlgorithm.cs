﻿using BLL.Abstractions;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using BLL.ReservationManager.Abstractions;
using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Branch_0
{
    public class ConfirmReservationDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 0;
        public int AlgorithmStepId { get; } = 6;

        private string _messageToClient { get; set; } = $"Отлично! Подтвердите бронирование.";

        private readonly AnswerType _answerType = AnswerType.Button;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;
        private readonly IReservationHandler _reservationHandler;

        private readonly IBotMessageSender _messageSender;
        private readonly IDbService<Reservation> _reservationService;
        public ConfirmReservationDialogAlgorithm(IBotMessageSender messageSender, IDbService<Reservation> reservationService,
            IReservationHandler reservationHandler)
        {
            _reservationService = reservationService;
            _reservationHandler = reservationHandler;
            _messageSender = messageSender;
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                {"Забронировать", 0},
                {"Отмена", 1}
            };
        }

        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState dialogState)
        {
            var buttonTypeAnswer = new ButtonTypeAnswer
            {
                Answer = dialogState.ClientAnswer,
                AnswersCollection = _answerAndAlgorithmBranch
                .Select(a => a.Key)
                .ToArray()
            };

            if (!AnswerTypeHandling.AnswerTypeGuard.AnswerIsCorrect(_answerType, buttonTypeAnswer))
            {
                await _messageSender.Send(dialogState.DialogChatId, "Такого варианта ответа нет, ответьте нажатием на кнопку", null);
                return null;
            }

            var algorithmBranchByAnswer = _answerAndAlgorithmBranch
                .FirstOrDefault(o => o.Key == dialogState.ClientAnswer)
                .Value;

            var reservation = await _reservationService.Get(r => r.Client.ChatId == dialogState.DialogChatId
                && !r.Confirmed);

            if (reservation == null)
                return null;

            dialogState.DialogBranchId = algorithmBranchByAnswer;

            if(dialogState.ClientAnswer == "Забронировать")        
                await _reservationHandler.ConfirmReservation(reservation);

            return dialogState;
        }

        public async Task<string> GetMessageToClient(CurrentDialogState currentDialogState)
        {
            var reservations = await _reservationService.GetMany(r => r.Client.ChatId == currentDialogState.DialogChatId);

            var reservation = reservations.FirstOrDefault(rest=> rest.CreationDate == reservations.Max(r => r.CreationDate));

            var textToAdd = @$"{"\n"}Ресторан: {reservation.Restaurant.Name}" +
                            @$"{"\n"}Дата и время: {reservation.Time.Date} в {reservation.Time.Hour}.{reservation.Time.Minute}";

            _messageToClient += textToAdd;

            return _messageToClient;
        }

        public Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState currentDialogState)
        {
            var keyboardButtons = new List<KeyboardButton>();
            foreach (var option in _answerAndAlgorithmBranch)
            {
                var newKeyboardButton = new KeyboardButton(option.Key);
                keyboardButtons.Add(newKeyboardButton);
            }

            var replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardButtons, true);

            return Task.FromResult<ReplyKeyboardMarkup>(replyKeyboardMarkup);
        }
    }
}
