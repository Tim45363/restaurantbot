﻿using BLL.Abstractions;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using BLL.Models.Enums;
using BLL.ReservationManager.Abstractions;
using BLL.ReservationManager.Models;
using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Branch_0
{
    public class ReservationTimeDialogAlgorithm : IDialogAlgorithm
    {
        public int AlgorithmBranchId { get; } = 0;
        public int AlgorithmStepId { get; } = 5;

        private string _messageToClient { get; set; } = $"Выбирете время";

        private readonly AnswerType _answerType = AnswerType.Button;

        private readonly Dictionary<string, int> _answerAndAlgorithmBranch;
        private readonly IDbService<Reservation> _reservationService;
        private readonly IReservationHandler _reservationHandler;

        private readonly IBotMessageSender _messageSender;
        public ReservationTimeDialogAlgorithm(IBotMessageSender messageSender, IDbService<Reservation> reservationService,
            IReservationHandler reservationHandler)
        {
            _messageSender = messageSender;
            _reservationService = reservationService;
            _reservationHandler = reservationHandler;
            _answerAndAlgorithmBranch = new Dictionary<string, int>
            {
                //значит любой ответ примим
                { string.Empty, 1}
            };
        }

        public async Task<CurrentDialogState> HandleAnswerFromClient(CurrentDialogState currentDialogState)
        {

            var reservation = await _reservationService.Get(r => r.Client.ChatId == currentDialogState.DialogChatId
             && !r.Confirmed);

            var reservationModel = new ReservationModel
            {
                RestaurantName = reservation.Restaurant.Name,
                ReservationDurationInMinutes = reservation.DurationInMinutes,
                ReservationTime = reservation.Time
            };

            var freeTimes = await _reservationHandler.GetFreeTimeScheduleByDay(reservationModel);


            var buttonTypeAnswer = new ButtonTypeAnswer
            {
                Answer = currentDialogState.ClientAnswer,
                AnswersCollection = freeTimes.Keys.Select(freeTime => freeTime.Hour.ToString() + ':' + freeTime.Minute.ToString()).ToArray()
            };

            if (!AnswerTypeHandling.AnswerTypeGuard.AnswerIsCorrect(_answerType, buttonTypeAnswer))
            {
                await _messageSender.Send(currentDialogState.DialogChatId, "Время уже занято", null);
                return null;
            }

            reservation.Time = reservation.Time
                .AddHours(Int32.Parse(currentDialogState.ClientAnswer.Split(":")[0]))
                .AddMinutes(Int32.Parse(currentDialogState.ClientAnswer.Split(":")[1]));

            await _reservationService.Update(reservation);

            return currentDialogState;
        }

        public Task<string> GetMessageToClient(CurrentDialogState currentDialogState)
        {
            return Task.FromResult<string>(_messageToClient);
        }

        public async Task<ReplyKeyboardMarkup> GetKeyboardMarkup(CurrentDialogState currentDialogState)
        {
            var keyboardButtons = new List<KeyboardButton>();

            var reservation = await _reservationService.Get(r => r.Client.ChatId == currentDialogState.DialogChatId 
            && !r.Confirmed);

            var reservationModel = new ReservationModel
            {
                RestaurantName = reservation.Restaurant.Name,
                ReservationDurationInMinutes = reservation.DurationInMinutes,
                ReservationTime = reservation.Time
            };

            var freeTimes = await _reservationHandler.GetFreeTimeScheduleByDay(reservationModel);

            //var counter = 0;
            foreach (var freeTime in freeTimes.Keys)
            {
                var newKeyboardButton = new KeyboardButton(freeTime.Hour.ToString()+ ':' +freeTime.Minute.ToString());
                keyboardButtons.Add(newKeyboardButton);
            }

            var replyKeyboardMarkup = new ReplyKeyboardMarkup(keyboardButtons, true);

            return replyKeyboardMarkup;
        }
    }
}
