﻿using System;
using BLL.Abstractions;
using BLL.BotDialogProcessing.Abstractions;
using DLL.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.BotDialogProcessing.DialogAlgorithms.Abstractions;
using BLL.BotDialogProcessing.DialogAlgorithms.GreetingDialogTemplate_1;
using BLL.BotMessageSender.Abstractions;
using BLL.Models;
using Microsoft.Extensions.Logging;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;
using BLL.TelegramUpdateHandler.Abstractions;
using BLL.Models.Enums;
using BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2;
using BLL.ReservationManager;
using BLL.ReservationManager.Abstractions;

namespace BLL.BotDialogProcessing
{
    public class DialogManager : IDialogManager
    {
        private readonly IDbService<Client> _clientsService;
        private readonly IDbService<CurrentDialog> _currentDialogService;
        //private readonly ITelegramUpdateHandler _updateHandler;
        private readonly IBotMessageSender _messageSender;
        private readonly ILogger<DialogManager> _logger;
        private readonly List<IDialogTemplate> _dialogTemplates;
        
        public DialogManager(IDbService<Client> clientService,
            IDbService<CurrentDialog> currentDialogService,
            IDbService<Reservation> reservationService,
             IDbService<City> cityService,
            IReservationHandler reservationHandler,
            IBotMessageSender messageSender,
            IDbService<Restaurant> restaurantService,
            ILogger<DialogManager> logger)
        {
            _clientsService = clientService;
            _messageSender = messageSender;
            _logger = logger;
            _currentDialogService = currentDialogService;
            _dialogTemplates = new List<IDialogTemplate>
            {
                new GreetingDialogTemplate(messageSender, clientService, cityService, restaurantService, currentDialogService),
                new ReservationDialogTemplate(messageSender, restaurantService, clientService, reservationHandler,
                reservationService, currentDialogService)
            };
        }

        /// <summary>
        /// Создать текущий диалог в базе
        /// </summary>
        /// <param name="dialogTemplateId"></param>
        /// <param name="client"></param>
        /// <param name="algorithmId"></param>
        /// <param name="branchId"></param>
        /// <returns></returns>
        public async Task<CurrentDialog> CreateCurrentDialog(int dialogTemplateId, Client client, int algorithmId, int branchId = 0)
        {
            var currentDialogEntity = new CurrentDialog
            {
                DialogTemplateId = dialogTemplateId,
                Client = client,
                AlgorithmId = algorithmId,
                AlgorithmBranchId = branchId,
            };
            
            await _currentDialogService.Create(currentDialogEntity);

            return currentDialogEntity;
        }

        public async Task DeleteCurrentDialog(int currentDialogId)
        {
            await _currentDialogService.Delete(currentDialogId);
        }

        public async Task<bool> DoesItForMe(Update data)
        {
            var client = await _clientsService.Get(c => c.ChatId == data.Message.Chat.Id);
            var existingDialog1 = await _currentDialogService.Get(d => d.IsActive);
            var existingDialog = await _currentDialogService.Get(d => d.Client.Id == client.Id);

            var startDialog = _dialogTemplates.FirstOrDefault(d => d.Command == data.Message.Text);

            if (startDialog != null || existingDialog != null)
                return true;

            // Если диалог существует или ответ пользователя это команда для старта диалога, то тогда возвращаем true, если нет, то нет
            return false;
        }

        /// <summary>
        /// Отправляет клиенту базовые кнопки 
        /// </summary>
        /// <returns></returns>
        private async Task PutDalogOnNeutralStep(long chatId)
        {
            // TODO: Дополнить маркап, а лучше сделать статик класс, где есть маркапы на все случаи жизни

            var keyboardMarkupForTemplates = MarkupHandler.GetMarkupForTemplates(_dialogTemplates);

            await _messageSender.Send(chatId, "Вы можете забронировать столик в ресторане не покидая телеграм!", keyboardMarkupForTemplates);

        }

        /// <summary>
        /// </summary>
        /// <param name="update"></param>
        /// <returns></returns>
        public async Task<bool> Manage(Update update)
        {
            // Определяем, есть ли уже существующий диалог или надо начать новый

            var client = await _clientsService.Get(c=> c.ChatId == update.Message.Chat.Id);

            var currentDialog = await _currentDialogService.Get(c => c.Client.ChatId == update.Message.Chat.Id);

            if(currentDialog == null)
            {
                var newCurrentDialog = await StartNewTemplate(update, client);
                var newCurrentTemplate = _dialogTemplates
                .FirstOrDefault(d => d.DialogTemplateId == newCurrentDialog.DialogTemplateId);
                var newCurrentAlgorithm = await newCurrentTemplate.GetCurrentAlgorithm(newCurrentDialog.Client.ChatId);
                var newCurrentDialogState1 = new CurrentDialogState
                {
                    DialogTemplateId = newCurrentDialog.DialogTemplateId,
                    DialogBranchId = newCurrentAlgorithm.AlgorithmBranchId,
                    DialogStepId = newCurrentAlgorithm.AlgorithmStepId,
                    ClientAnswer = update.Message.Text,
                    DialogChatId = update.Message.Chat.Id
                };
                await newCurrentTemplate.SendNextQuestion(newCurrentDialogState1);
                await SaveDialogChanges(newCurrentDialogState1, newCurrentDialog);

                return true;
            }
                
            // Если есть, то вытаскиваем из коллекции нужный нам темплейт
            var currentTemplate = _dialogTemplates
                .FirstOrDefault(d => d.DialogTemplateId == currentDialog.DialogTemplateId);

            var isGoneBack = await GoBackIfNeed(update, currentTemplate);

            //если true вернулся назад
            if (isGoneBack)
                return true; 

            var currentAlgorithm = await currentTemplate.GetCurrentAlgorithm(currentDialog.Client.ChatId);

            var branchBeforeHandle = currentAlgorithm.AlgorithmBranchId;
            var templateBeforeHandle = currentTemplate.DialogTemplateId;

            var currentDialogState = new CurrentDialogState
            {
                DialogBranchId = currentAlgorithm.AlgorithmBranchId,
                DialogStepId = currentAlgorithm.AlgorithmStepId,
                ClientAnswer = update.Message.Text,
                DialogTemplateId = currentTemplate.DialogTemplateId,
                DialogChatId = update.Message.Chat.Id
            };

            var newCurrentDialogState = await HandleAnswer(currentDialogState, currentAlgorithm);

            //Диалог закончен, нужно отправить кнопки по умолчанию
            if (newCurrentDialogState.DialogBranchId == -1)
            {
                await DeleteCurrentDialog(currentDialog.Id);
                await PutDalogOnNeutralStep(currentDialog.Client.ChatId);
                return true;
            }

            //Ответ не верный 
            if (newCurrentDialogState == null)
            {
                await SaveDialogChanges(currentDialogState, currentDialog);
                return true;
            }

            newCurrentDialogState.DialogStepId = currentTemplate
                .GetFirstAlgorithmOfNewBranch(newCurrentDialogState.DialogBranchId);

            //Если шаг был последним, и мы не будем ждать ответа клиента
            if (currentTemplate.LastDialogStepsId.Contains(newCurrentDialogState.DialogStepId))
            {
                await currentTemplate.SendNextQuestion(newCurrentDialogState);
                await DeleteCurrentDialog(currentDialog.Id);
                await PutDalogOnNeutralStep(currentDialog.Client.ChatId);
                return true;
            }

            //если template изменился
            if (templateBeforeHandle != newCurrentDialogState.DialogTemplateId)
            {
                await ChangeTemplate(newCurrentDialogState);
                await SaveDialogChanges(newCurrentDialogState, currentDialog);
                return true;
            }

            //если бранч изменилась
            if (branchBeforeHandle != newCurrentDialogState.DialogBranchId)
            {
                newCurrentDialogState.DialogStepId = currentTemplate
                    .GetFirstAlgorithmOfNewBranch(newCurrentDialogState.DialogBranchId);
                newCurrentDialogState.DialogStepId--;
            }

            newCurrentDialogState.DialogStepId++;

            await currentTemplate.SendNextQuestion(newCurrentDialogState);
             
            await SaveDialogChanges(newCurrentDialogState, currentDialog);

            return true;
        }

        private async Task<CurrentDialogState> HandleAnswer(CurrentDialogState currentDialogState, IDialogAlgorithm currentAlgorithm)
        {
            try
            {
                var newCurrentDialogState = await currentAlgorithm.HandleAnswerFromClient(currentDialogState);
                return newCurrentDialogState;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message, e);
                return null;
            }

        }

        private async Task<CurrentDialog> StartNewTemplate(Update update, Client client)
        {
            var templateFoundByCommand = _dialogTemplates.FirstOrDefault(d => d.Command == update.Message.Text);
            if (templateFoundByCommand == null)
            {
                var message = "Такой команды не существует";
                _logger.LogInformation(message);
                await _messageSender.Send(update.Message.Chat.Id, message, null);
                return null;
            }

            var currentDialog = await CreateCurrentDialog(templateFoundByCommand.DialogTemplateId, client, 1);

            return currentDialog;
        }


        private async Task<bool> GoBackIfNeed(Update update, IDialogTemplate currentTemplate)
        {
            if (update.Message.Text == StaticAnswerOptions.Back)
            {
                var successfully = await currentTemplate.GoBackIfYouCan(update.Message.Chat.Id);
                if (successfully)
                    return true;

                await _messageSender.Send(update.Message.Chat.Id, "Не удалось вернуться на шаг назад", null);
                return true;
            }
            return false;
        }

        private async Task ChangeTemplate(CurrentDialogState newCurrentDialogState)
        {
            newCurrentDialogState.DialogBranchId = 0;
            newCurrentDialogState.DialogStepId = 1;
            var currentTemplate = _dialogTemplates
                .FirstOrDefault(d => d.DialogTemplateId == newCurrentDialogState.DialogTemplateId);
            
            //Отправляет первое сообщение, ждем следующего update чтобы обработать ответ. То есть больше мы ничего не можем сделать
            await currentTemplate.SendNextQuestion(newCurrentDialogState);
        }

        /// <summary>
        /// Переносит данные из CurrentDialogState в CurrentDialog и обнавляет CurrentDialog в базе данных
        /// </summary>
        /// <param name="currentDialogState"></param>
        /// <param name="currentDialog"></param>
        /// <returns></returns>
        private async Task SaveDialogChanges(CurrentDialogState currentDialogState, CurrentDialog currentDialog)
        {
            currentDialog.AlgorithmBranchId = currentDialogState.DialogBranchId;

            currentDialog.AlgorithmId = currentDialogState.DialogStepId;

            currentDialog.DialogTemplateId = currentDialogState.DialogTemplateId;

            await _currentDialogService.Update(currentDialog);
        }
    }
}
