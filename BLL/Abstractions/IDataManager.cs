using System.Threading.Tasks;

namespace BLL.Abstractions
{
    public interface IDataManager<TData, TResult>
    {
        /// <summary>
        /// Смогу ли я как менеджер эту информацию обработать, для меня ли она?
        /// </summary>
        /// <param name="data">Пришедшие данные</param>
        /// <typeparam name="TData">Тип данных</typeparam>
        /// <returns></returns>
        Task<bool> DoesItForMe(TData data);
        
        /// <summary>
        /// Начни делать что-то с данными
        /// </summary>
        /// <param name="data">Пришедшие данные</param>
        /// <returns></returns>
        Task<TResult> Manage(TData data);
    }
}