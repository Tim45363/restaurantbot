﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Abstractions
{
    public interface IDbService<TEntity>
    {
        public Task Create(TEntity entity);
        public Task<TEntity> Get(int id);
        public Task Update(TEntity message);
        public Task<List<TEntity>> GetMany(Func<TEntity, bool> predicate);
        public Task Delete(long id);
        public Task CreateMany(IEnumerable<TEntity> messages);
        public Task<TEntity> Get(Func<TEntity, bool> predicate);
    }
}
