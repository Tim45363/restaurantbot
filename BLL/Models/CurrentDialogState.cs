namespace BLL.Models
{
    //Не нарушаем инкапсуляцию
    /// <summary>
    /// Содержит переменные которые активно используются в приложении
    /// </summary>
    public sealed class CurrentDialogState
    {
        public string ClientAnswer { get; set; }
        public int DialogBranchId { get; set; }
        public int DialogStepId { get; set; }
        
        public int DialogTemplateId { get; set; }
        public long DialogChatId { get; set; }
    }
}