namespace BLL.Models.Enums
{
    public enum AnswerType
    {
        Text = 1,
        Button = 2,
        Phone = 3,
        Empty = 4,
        Date = 5,
        Email = 6,
        Unknown = 0
    }
}