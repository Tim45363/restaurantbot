using DLL.Entities;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace BLL.TelegramUpdateHandler.Abstractions
{
    public interface ITelegramUpdateHandler
    {
        Task Handle(Update update);
        public Task<Client> GetClient(long chatId, string name);

    }
}