using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Abstractions;
using BLL.BotDialogProcessing.Abstractions;
using BLL.TelegramUpdateHandler.Abstractions;
using DLL.Entities;
using Microsoft.Extensions.Logging;
using Telegram.Bot.Types;

namespace BLL.TelegramUpdateHandler
{
    public sealed class TelegramUpdateHandler : ITelegramUpdateHandler
    {
        private readonly IDialogManager _dialogManager;
        private int _updateMessageId;
        private readonly ILogger<TelegramUpdateHandler> _logger;
        private readonly IDbService<Client> _clientsService;
        private readonly IDbService<Country> _countryService;
        private readonly IDbService<Table> _tableService;
        private readonly IDbService<Restaurant> _restaurantService;
        private readonly IDbService<Manager> _managerService;
        private readonly IDbService<UpdateMessage> _updateMessageService;
        private readonly IDbService<City> _cityService;

        public TelegramUpdateHandler(
            IDialogManager dialogManager, 
            ILogger<TelegramUpdateHandler> logger,
            IDbService<Restaurant> restaurantService,
            IDbService<UpdateMessage> updateMessageService,
            IDbService<Client> clientsService,
            IDbService<Table> tableService,
            IDbService<Manager> managerService,
            IDbService<Country> countryService,
            IDbService<City> cityService)
        {
            _countryService = countryService;
            _cityService = cityService;
            _tableService = tableService;
            _clientsService = clientsService;
            _dialogManager = dialogManager;
            _managerService = managerService;
            _restaurantService = restaurantService;
            _logger = logger;
            _updateMessageService = updateMessageService;
        }


        public async Task<Client> GetClient(long chatId, string name)
        {


            //если есть 
            var foundClient = await _clientsService.Get(c => c.ChatId == chatId);
            if (foundClient != null)
                return foundClient;

            //тогда создаем 
            var newClient = new Client
            {
                ChatId = chatId,
                Name = name
            };

            await _clientsService.Create(newClient);
            return newClient;
        }

        private async Task DeleteAllData()
        {

        }

        private async Task SetUpMockData()
        {
            var isCountryCreated = await _countryService.Get(c => c.Name == "Россия");

            var country = new Country { Name = "Ташкент" };

            await _countryService.Create(country);
            var newCountry = await _countryService.Get(c => c.Name == country.Name);

            var city = new City { Name = "Орёл", Country = newCountry };
            await _cityService.Create(city);
            var newCity = await _cityService.Get(c => c.Name == city.Name);

            var manager = new Manager { Name = "Tokio" };
            await _managerService.Create(manager);
            var newManager = await _managerService.Get(m => m.Name == manager.Name);

            var tables = new List<Table> { new Table { Number = 1, PlaceCount = 3 } };
            await _tableService.CreateMany(tables);

            var newTables = await _tableService.GetMany(t => t.IsActive);

            var restaurans = new List<Restaurant>{ new Restaurant {Name = "Shalda" } };

            await _restaurantService.CreateMany(restaurans);

            var restaurant = await _restaurantService.Get(r => r.Name == "Shalda");

            restaurant.Managers = new List<Manager>() { newManager };
            restaurant.Tables = newTables;
            restaurant.City = newCity;
            restaurant.Tables = newTables;

            await _restaurantService.Update(restaurant);
        }

        private async Task WipeOutAllClientsToHell()
        {
            var clients = await _clientsService.GetMany(c => c.IsActive);
            clients.ForEach(async c => await _clientsService.Delete(c.Id));

        }

        /// <summary>
        /// Если update найден, значит этот update уже обрабатывается и повторно обрабатывать его не нужно 
        /// Мы это делаем потому что сервис телеграма присылает одинаковые апдейты автоматной очередью пока api не вернёт 200 ok()
        /// Поэтому если update не найден мы создаем его в ДБ и возвращаем false, то есть данный update пришел первый раз и его нужно обработать 
        /// </summary>
        /// <param name="updateId"></param>
        /// <returns></returns>
        private async Task<bool> IsUpdateExists(int updateId, int chatId)
        {
            var foundUpdateMessage = await _updateMessageService.Get(u => u.MessageId == updateId);
            if (foundUpdateMessage != null)
                return true;

            await _updateMessageService.Create(new UpdateMessage { MessageId = updateId, ChatId = chatId });

            return false;
        }

        public async Task Handle(Update update)
        {

            await SetUpMockData();

            var rest = await _restaurantService.GetMany(r => r.IsActive);

            //await WipeOutAllClientsToHell();

            var updateExists = await IsUpdateExists(update.Id, (int)update.Message.Chat.Id);

            if (updateExists)
                return;

            await GetClient(update.Message.Chat.Id, update.Message.From.FirstName + update.Message.From.LastName);

            var doesItForMe = await _dialogManager.DoesItForMe(update);

            if (doesItForMe)
                await _dialogManager.Manage(update);

            // Дальше должны другие менеджеры начать работать

            await DeleteUpdateMessage(update.Id);
        }

        private async Task DeleteUpdateMessage(int chatId)
        {
            var foundUpdateMessage = await _updateMessageService.GetMany(u => u.ChatId == chatId);
            foundUpdateMessage.ForEach(async m => await _updateMessageService.Delete(m.ChatId));
        }
    }
}