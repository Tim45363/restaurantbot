using System;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using BLL.AnswerTypeGuard.Models;
using BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Services;
using BLL.Models.Enums;
using Shared.Extensions;

namespace BLL.AnswerTypeHandling
{
    public static class AnswerTypeGuard
    {
        public static bool AnswerIsCorrect<TAnswer>(AnswerType answerType, TAnswer answer)
        {
            switch (answerType)
            {
                case AnswerType.Email:
                    return IsEmailCorrect((answer as ButtonTypeAnswer).Answer);
                case AnswerType.Text:
                    return true;
                case AnswerType.Button:
                    return IsButtonAnswerCorrect(answer as ButtonTypeAnswer);
                case AnswerType.Phone:
                    return IsPhoneCorrect((answer as ButtonTypeAnswer).Answer);
                case AnswerType.Empty:
                    return answer == null;
                case AnswerType.Date:
                    return IsDateCorrect((answer as ButtonTypeAnswer).Answer);
                default:
                    throw new Exception($"Не удалось найти тип ответа из enum для следующего: {answerType.ToString()}");
            }

            return false;
        }

        private static bool IsDateCorrect(string dateToCheck)
        {
            //string[] formats = {"M/d/yyyy h:mm:ss tt", "M/d/yyyy h:mm tt",
            //             "MM/dd/yyyy hh:mm:ss", "M/d/yyyy h:mm:ss",
            //             "M/d/yyyy hh:mm tt", "M/d/yyyy hh tt",
            //             "M/d/yyyy h:mm", "M/d/yyyy h:mm",
            //             "MM/dd/yyyy hh:mm", "M/dd/yyyy hh:mm"};
            //string[] dateStrings = {"5/1/2009 6:32 PM", "05/01/2009 6:32:05 PM",
            //                  "5/1/2009 6:32:00", "05/01/2009 06:32",
            //                  "05/01/2009 06:32:00 PM", "05/01/2009 06:32:00"};
            //DateTime dateValue;

            //var isDateCorrect = DateTime.TryParseExact(dateToCheck, formats,
            //    new CultureInfo("ru - RU"), DateTimeStyles.None, out dateValue);

            var parsedDate = ParseMessageIntoDate.TryParseDateToString(dateToCheck);

            if (parsedDate == null)
                return false;

            return true;
        }

        private static bool IsPhoneCorrect(string phone)
        {
            return Regex.IsMatch(phone, @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        private static bool IsEmailCorrect(string emailaddress)
        {
            MailAddress email;

            return MailAddress.TryCreate(emailaddress, out email);
        }

        /// <summary>
        /// Тоже как пример, но уже более рабочий и демонстрирующий как можно использовать дженерик в наших целях
        /// </summary>
        /// <param name="buttonAnswer"></param>
        /// <returns></returns>
        private static bool IsButtonAnswerCorrect(ButtonTypeAnswer buttonAnswer)
        {
            if (buttonAnswer.Answer.IsNullOrEmpty())
                return false;
            if (buttonAnswer.AnswersCollection == null)
                throw new Exception($"Для ответа {buttonAnswer.Answer} не пришли варианты кнопок");
            
            var correct = buttonAnswer.AnswersCollection.Any(a => a == buttonAnswer.Answer);
            return correct;
        }
    }
}