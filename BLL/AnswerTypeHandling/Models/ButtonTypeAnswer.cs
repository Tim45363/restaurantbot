namespace BLL.AnswerTypeGuard.Models
{
    public sealed class ButtonTypeAnswer
    {
        public string Answer { get; set; }
        public string[] AnswersCollection { get; set; }
    }
}