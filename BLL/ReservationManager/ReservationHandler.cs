﻿using BLL.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.ReservationManager.Abstractions;
using BLL.ReservationManager.Models;
using DLL.Entities;
using Hangfire;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ReservationManager
{
    public class ReservationHandler : IReservationHandler
    {
        private readonly ILogger<ReservationHandler> _logger;
        private readonly IDbService<Restaurant> _restaurantService;
        private readonly IDbService<Table> _tableService;
        private readonly IDbService<Reservation> _reservationService;
        private readonly IDbService<Client> _clientService;
        private readonly IBotMessageSender _botMessageSender;

        public ReservationHandler(IDbService<Restaurant> restaurantService,
            ILogger<ReservationHandler> logger,
            IDbService<Table> tableService,
            IDbService<Reservation> reservationService,
            IBotMessageSender botMessageSender,
            IDbService<Client> clientService)
        {
            _botMessageSender = botMessageSender;
            _restaurantService = restaurantService;
            _clientService = clientService;
            _logger = logger;
            _tableService = tableService;
            _reservationService = reservationService;
        }

        public async Task<bool> ConfirmReservation(Reservation reservation)
        {
            var resaurant = await _restaurantService.Get(r => r.Name == reservation.Restaurant.Name);

            if (resaurant == null)
            {
                _logger.LogCritical("Ресторан не найден ");
                return false;
            }

            var endOfReservation = reservation.Time.AddMinutes(reservation.DurationInMinutes);
            var freeTables = GetFreeTables(reservation.Time, endOfReservation, resaurant.Tables, resaurant.Reservations,
                reservation.PeopleCount);

            if (freeTables.Count == 0)
                return false;

            
            if(reservation.Client == null)
            {
                _logger.LogCritical("Клиент не найден ");
                return false;
            }

            reservation.Confirmed = true;

            NotifyClientAboutBooking(reservation);

            await _reservationService.Update(reservation);

            return true;
        }

        private void NotifyClientAboutBooking(Reservation reservation)
        {
            var whenNotify = reservation.Time - TimeSpan.FromDays(1);

            var messageForClient = $"{reservation.Client.Name}, напоминаем что в {reservation.Time.DayOfWeek.ToString()} у Вас забронирован " +
            $"столик в ресторане {reservation.Restaurant.Name}";
 
            BackgroundJob.Schedule(() => _botMessageSender.Send(reservation.Client.ChatId, messageForClient, null), whenNotify);
        }

        public async Task<Dictionary<DateTime, DateTime>> GetFreeTimeScheduleByDay(ReservationModel reservationModel)
        {
            var resaurant = await _restaurantService.Get(r => r.Name == reservationModel.RestaurantName);

            if (resaurant == null)
            {
                _logger.LogCritical("Ресторан не найден ");
                return null;
            }


            var reservationsInDayList = resaurant.Reservations
                .Where(r => r.Time.DayOfYear == reservationModel.ReservationTime.DayOfYear)
                .ToList();

            //при условии что day == 00 h 00 min 00 c
            if(reservationModel.ReservationTime.Hour != 0 || reservationModel.ReservationTime.Minute != 0)
            {
                _logger.LogCritical(" DateTime day != 00 h 00 min 00 c. Неточное время ");
                return null;
            }

            var openingTime = reservationModel.ReservationTime + TimeSpan.FromHours(resaurant.OpeningHour);
            var closingTime = reservationModel.ReservationTime.AddHours(resaurant.ClosingHour);

            var allFreeReservationTimesInDay = new Dictionary<DateTime, DateTime>();

            //если время работы ресторана с 8.00 до 23.00 то потребуется 180 итераций 
            for (DateTime reservationStartTime = openingTime; reservationStartTime != closingTime;
                reservationStartTime += TimeSpan.FromMinutes(5))
            {
                var endOfReservationTime = reservationStartTime.AddMinutes(reservationModel.ReservationDurationInMinutes);
                var freeTables = GetFreeTables(reservationStartTime, endOfReservationTime, 
                    resaurant.Tables, reservationsInDayList);

                if (freeTables.Count != 0)
                    allFreeReservationTimesInDay.Add(reservationStartTime, endOfReservationTime);
            }

            return allFreeReservationTimesInDay;
        }

        public async Task<bool> IsDateTimeFree(ReservationModel timeToCheck)
        {
            var resaurant = await _restaurantService.Get(r => r.Name == timeToCheck.RestaurantName);

            if (resaurant == null)
            {
                _logger.LogCritical("Ресторан не найден ");
                return false;
            }
            //reservationTime -= TimeSpan.FromSeconds(180);
            var endOfReservation = timeToCheck.ReservationTime.AddMinutes(timeToCheck.ReservationDurationInMinutes);
            var reservations = resaurant.Reservations;

            var freeTables = GetFreeTables(timeToCheck.ReservationTime, endOfReservation, resaurant.Tables, reservations);

            return freeTables.Any();
        }

        private List<Table> GetFreeTables(DateTime startReservationTime,
            DateTime endReservationTime, List<Table> tables, IEnumerable<Reservation> reservations, int peopleCount = 1)
        {
            var freeTables = new List<Table>();

            foreach(var table in tables)
            {
                var reservationsOnSameTable = reservations.Where(r => r.Table.Number == table.Number)
                    .ToList();

                var freeTable = true;

                foreach (var r in reservationsOnSameTable)
                {
                    var oldBegin = r.Time;
                    var oldEnd = oldBegin.AddMinutes(r.DurationInMinutes);

                    var o1 = (startReservationTime > oldBegin && startReservationTime < oldEnd);
                    var o2 = ((endReservationTime) > oldBegin);
                    var o3 = ((endReservationTime) < oldEnd);
                    var o4 = (oldBegin > startReservationTime);
                    var o5 = (oldBegin < (endReservationTime));
                    var o6 = (oldEnd > startReservationTime);
                    var o7 = (oldEnd < (endReservationTime));

                    bool notAllowed = (o1 || (o2 && o3)) || ((o4 && o5) || (o6 && o7));

                    if (notAllowed)
                        freeTable = false;
                }

                if (freeTable && (peopleCount <= table.PlaceCount))
                    freeTables.Add(table);
            }

            return freeTables;
        }
    }
}
