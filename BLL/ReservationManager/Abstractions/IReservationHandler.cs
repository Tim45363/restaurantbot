﻿using BLL.ReservationManager.Models;
using DLL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ReservationManager.Abstractions
{
    public interface IReservationHandler
    {
        /// <summary>
        /// Возвращает true если указанное время резервации в ресторане свободно
        /// </summary>
        /// <param name="timeToCheck"></param>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        public Task<bool> IsDateTimeFree(ReservationModel timeToCheck);

        /// <summary>
        /// Возвращает true если резервация прошла успешно 
        /// </summary>
        /// <param name="reservationTime"></param>
        /// <param name="restaurant"></param>
        /// <param name="chatId"></param>
        /// <returns></returns>
        public Task<bool> ConfirmReservation(Reservation reservation);

        /// <summary>
        /// Возвращает словарь(время и длительность) свободного времени в заданный день
        /// При условии что day == 00 h 00 min 00 c
        /// </summary>
        /// <param name="restaurant"> Ресторан </param>
        /// <param name="day"> День резервации столика </param>
        /// <returns></returns>
        public Task<Dictionary<DateTime, DateTime>> GetFreeTimeScheduleByDay(ReservationModel reservationModel);
    }
}
