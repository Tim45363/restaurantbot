﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.ReservationManager.Models
{
    /// <summary>
    /// Вспомогательная модель для упрощения коммуникации с ReservationHandler 
    /// </summary>
    public class ReservationModel
    {
        public DateTime ReservationTime { get; set; }
        public int ReservationDurationInMinutes { get; set; } = 60;
        public string RestaurantName { get; set; }
    }
}
