using System.Threading.Tasks;
using BLL.BotMessageSender.Abstractions;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotMessageSender
{
    public class BotMessageSender : IBotMessageSender
    {
        public async Task Send(long chatId, string message, ReplyKeyboardMarkup replyKeyboardMarkup)
        {
            var telegramClient = TelegramBotRequisites.GetClient();

            if (replyKeyboardMarkup == null  )
                await telegramClient.SendTextMessageAsync(chatId, message, replyMarkup: new ReplyKeyboardRemove());
            else
                await telegramClient.SendTextMessageAsync(chatId, message, replyMarkup: replyKeyboardMarkup);

        }
    }
}