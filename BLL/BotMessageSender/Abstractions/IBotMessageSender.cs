using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace BLL.BotMessageSender.Abstractions
{
    public interface IBotMessageSender
    {
        Task Send(long chatId, string message, ReplyKeyboardMarkup replyKeyboardMarkup);
    }
}