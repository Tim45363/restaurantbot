﻿using BLL.Abstractions;
using DLL.Abstractions;
using DLL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TableDbService : IDbService<Table>
    {
        private readonly IDbRepository<Table> _repository;
        private ILogger<TableDbService> _logger;

        public TableDbService(IDbRepository<Table> dbRepository, ILogger<TableDbService> logger)
        {
            _logger = logger;
            _repository = dbRepository;
        }
        public async Task Create(Table regularQuestion)
        {
            try
            {
                await _repository.CreateAsync(regularQuestion);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }

        public async Task<Table> Get(Func<Table, bool> predicate)
        {
            try
            {
                var regularQuestions = _repository.Get(predicate);
                var regularQuestion = regularQuestions.ToList().FirstOrDefault();
                if (regularQuestion == null)
                    _logger.LogCritical("Объект Table не найден");
                return regularQuestion;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }
        public async Task<List<Table>> GetMany(Func<Table, bool> predicate)
        {
            try
            {
                var regularQuestions = _repository.Get(predicate);
                var regularQuestionsList = regularQuestions
                  .ToList();

                return regularQuestionsList;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }


        /// <summary>
        /// получает объект
        /// </summary>
        /// <returns></returns>

        public async Task<Table> Get(int id)
        {
            try
            {
                var regularQuestions = _repository.Get(e => e.Id == id).ToList();
                var regularQuestion = regularQuestions.FirstOrDefault();
                if (regularQuestion == null)
                    _logger.LogCritical("Объект Table не найден");
                return regularQuestion;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }


        /// <summary>
        /// обновляет объект
        /// </summary>
        /// <returns></returns>

        public async Task Update(Table manager)
        {
            try
            {
                _repository.Update(manager);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        /// <summary>
        /// удалает объект
        /// </summary>
        /// <returns></returns>

        public async Task Delete(long id)
        {
            try
            {
                var managers = _repository.Get(e => e.Id == id);
                _repository.Delete(managers.FirstOrDefault());
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        public async Task CreateMany(IEnumerable<Table> manager)
        {
            try
            {
                await _repository.CreateManyAsync(manager);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
    }
}
