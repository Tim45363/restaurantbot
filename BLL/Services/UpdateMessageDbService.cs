﻿using BLL.Abstractions;
using DLL.Abstractions;
using DLL.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UpdateMessageDbService : IDbService<UpdateMessage>
    {
        private readonly IDbRepository<UpdateMessage> _repository;
        private ILogger<UpdateMessageDbService> _logger;
        public UpdateMessageDbService(IDbRepository<UpdateMessage> dbRepository, ILogger<UpdateMessageDbService> logger)
        {
            _logger = logger;
            _repository = dbRepository;
        }
        public async Task Create(UpdateMessage updateMessage)
        {
            try
            {
                await _repository.CreateAsync(updateMessage);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }

        public async Task<UpdateMessage> Get(Func<UpdateMessage, bool> predicate)
        {
            try
            {
                var updateMessages = _repository.Get(predicate);
                var updateMessage = updateMessages.ToList().FirstOrDefault();
                if (updateMessage == null)
                    return null;
                return updateMessage;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }
        public async Task<List<UpdateMessage>> GetMany(Func<UpdateMessage, bool> predicate)
        {
            try
            {
                var updateMessages = _repository.Get(predicate);
                var updateMessage = updateMessages.ToList();
                return updateMessage;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }


        /// <summary>
        /// получает объект
        /// </summary>
        /// <returns></returns>

        public async Task<UpdateMessage> Get(int id)
        {
            try
            {
                var updateMessages = _repository.Get(e => e.Id == id).ToList();
                var updateMessage = updateMessages.FirstOrDefault();
                if (updateMessage == null)
                    throw new Exception("Объект не найден");
                return updateMessage;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }



        /// <summary>
        /// обновляет объект
        /// </summary>
        /// <returns></returns>

        public async Task Update(UpdateMessage updateMessage)
        {
            try
            {
                _repository.Update(updateMessage);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        /// <summary>
        /// удалает объект
        /// </summary>
        /// <returns></returns>

        public async Task Delete(long id)
        {
            try
            {
                var updateMessages = _repository.Get(e => e.Id == id);
                _repository.Delete(updateMessages.FirstOrDefault());
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        public async Task CreateMany(IEnumerable<UpdateMessage> updateMessages)
        {
            try
            {
                await _repository.CreateManyAsync(updateMessages);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
    }
}
