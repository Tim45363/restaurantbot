﻿using BLL.Abstractions;
using DLL.Abstractions;
using DLL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class HallDbService : IDbService<Hall>
    {
        private readonly IDbRepository<Hall> _repository;
        private ILogger<HallDbService> _logger;

        public HallDbService(IDbRepository<Hall> dbRepository, ILogger<HallDbService> logger)
        {
            _logger  = logger;
            _repository = dbRepository;
        }
        public async Task Create(Hall hall)
        {
            try
            {
                await _repository.CreateAsync(hall);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }

        public async Task<Hall> Get(Func<Hall, bool> predicate)
        {
            try
            {
                var halls = _repository.Get(predicate);
                var hall = halls.ToList().FirstOrDefault();
                if (hall == null)
                    _logger.LogCritical("Объект Hall не найден");
                return hall;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }
        public async Task<List<Hall>> GetMany(Func<Hall, bool> predicate)
        {
            try
            {
                var halls = _repository.Get(predicate);
                var hallsList = halls.ToList();
                return hallsList;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }


        /// <summary>
        /// получает объект
        /// </summary>
        /// <returns></returns>

        public async Task<Hall> Get(int id)
        {
            try
            {
                var halls = _repository.Get(e => e.Id == id).ToList();
                var hall = halls.FirstOrDefault();
                if (hall == null)
                    _logger.LogCritical("Объект Hall не найден");
                return hall;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }



        /// <summary>
        /// обновляет объект
        /// </summary>
        /// <returns></returns>

        public async Task Update(Hall hall)
        {
            try
            {
                _repository.Update(hall);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        /// <summary>
        /// удалает объект
        /// </summary>
        /// <returns></returns>

        public async Task Delete(long id)
        {
            try
            {
                var halls = _repository.Get(e => e.Id == id);
                _repository.Delete(halls.FirstOrDefault());
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        public async Task CreateMany(IEnumerable<Hall> halls)
        {
            try
            {
                await _repository.CreateManyAsync(halls);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
    }
}
