﻿using BLL.Abstractions;
using DLL.Abstractions;
using DLL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class RegularQuestionDbService : IDbService<RegularQuestion>
    {
        private readonly IDbRepository<RegularQuestion> _repository;
        private ILogger<RegularQuestionDbService> _logger;

        public RegularQuestionDbService(IDbRepository<RegularQuestion> dbRepository, ILogger<RegularQuestionDbService> logger)
        {
            _logger = logger;
            _repository = dbRepository;
        }
        public async Task Create(RegularQuestion regularQuestion)
        {
            try
            {
                await _repository.CreateAsync(regularQuestion);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }

        public async Task<RegularQuestion> Get(Func<RegularQuestion, bool> predicate)
        {
            try
            {
                var regularQuestions = _repository.Get(predicate);
                var regularQuestion = await regularQuestions.FirstOrDefaultAsync();
                if (regularQuestion == null)
                    _logger.LogCritical("Объект RegularQuestion не найден");
                return regularQuestion;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }
        public async Task<List<RegularQuestion>> GetMany(Func<RegularQuestion, bool> predicate)
        {
            try
            {
                var regularQuestions = _repository.Get(predicate);
                var regularQuestionsList = await regularQuestions.ToListAsync();
                return regularQuestionsList;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }


        /// <summary>
        /// получает объект
        /// </summary>
        /// <returns></returns>

        public async Task<RegularQuestion> Get(int id)
        {
            try
            {
                var regularQuestions = _repository.Get(e => e.Id == id).ToList();
                var regularQuestion = await regularQuestions.FirstOrDefaultAsync();
                if (regularQuestion == null)
                    _logger.LogCritical("Объект RegularQuestion не найден");
                return regularQuestion;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }



        /// <summary>
        /// обновляет объект
        /// </summary>
        /// <returns></returns>

        public async Task Update(RegularQuestion manager)
        {
            try
            {
                _repository.Update(manager);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        /// <summary>
        /// удалает объект
        /// </summary>
        /// <returns></returns>

        public async Task Delete(long id)
        {
            try
            {
                var managers = _repository.Get(e => e.Id == id);
                _repository.Delete(managers.FirstOrDefault());
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        public async Task CreateMany(IEnumerable<RegularQuestion> manager)
        {
            try
            {
                await _repository.CreateManyAsync(manager);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
    }
}
