﻿using BLL.Abstractions;
using DLL.Abstractions;
using DLL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CityDbService : IDbService<City>
    {
        private readonly IDbRepository<City> _repository;
        private readonly ILogger<CityDbService> _logger;
        public CityDbService(IDbRepository<City> dbRepository, ILogger<CityDbService> logger)
        {
            _repository = dbRepository;
            _logger = logger;
        }
        public async Task Create(City city)
        {
            try
            {
                await _repository.CreateAsync(city);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }

        public async Task<City> Get(Func<City, bool> predicate)
        {
            try
            {
                var cities = _repository.Get(predicate);
                var city = cities.ToList().FirstOrDefault();
                if (city == null)
                    throw new Exception("Объект не найден");
                return city;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }
        public async Task<List<City>> GetMany(Func<City, bool> predicate)
        {
            try
            {
                var cities = _repository.Get(predicate);
                var citiesList = cities.ToList();
                return citiesList;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }


        /// <summary>
        /// получает объект
        /// </summary>
        /// <returns></returns>

        public async Task<City> Get(int id)
        {
            try
            {
                var cities = _repository.Get(e => e.Id == id).ToList();
                var city = cities.FirstOrDefault();
                if (city == null)
                    _logger.LogCritical("Объект City не найден");

                return city;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }



        /// <summary>
        /// обновляет объект
        /// </summary>
        /// <returns></returns>

        public async Task Update(City bot)
        {
            try
            {
                _repository.Update(bot);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        /// <summary>
        /// удалает объект
        /// </summary>
        /// <returns></returns>

        public async Task Delete(long id)
        {
            try
            {
                var cities = _repository.Get(e => e.Id == id);
                _repository.Delete(cities.FirstOrDefault());
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        public async Task CreateMany(IEnumerable<City> cities)
        {
            try
            {
                await _repository.CreateManyAsync(cities);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
    }
}
