﻿using BLL.Abstractions;
using DLL.Abstractions;
using DLL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ClientDbService : IDbService<Client>
    {
        private readonly IDbRepository<Client> _repository;
        private readonly ILogger<ClientDbService> _logger;
        
        public ClientDbService(IDbRepository<Client> dbRepository, ILogger<ClientDbService> logger)
        {
            _logger = logger;
            _repository = dbRepository;
        }
        
        public async Task Create(Client client)
        {
            try
            {
                await _repository.CreateAsync(client);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }

        public async Task<Client> Get(Func<Client, bool> predicate)
        {
            try
            {
                // Тут мы сразу же инклудим резервации, чтобы иметь возможность работать с ними наверху
                // А так же делаем AsNoTracking, чтобы освободить объект от наблюдения со стороны EF

                // А если я хочу сохранить изменения Клиента? 
                var clients = await _repository
                    .Get(predicate)
                    .Include(c => c.Reservations)
                    .Include(c => c.City)
                    .ToListAsync();

                var client = clients.FirstOrDefault();

                // В этом случае вернем null, так как у нас при возврате есть обработчик null значения, в котором
                return client;
            }
            catch (Exception e)
            {
                // Все логирования ошибок осуществляем в формате Critical,
                // информэйшн только для информации, а тут мы должны понимать, что что-то плохое произошло
                _logger.LogCritical(e.Message);
                return null;
            }
        }
        public async Task<List<Client>> GetMany(Func<Client, bool> predicate)
        {
            try
            {
                var clients = await _repository.Get(predicate).ToListAsync();
                return clients;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }


        /// <summary>
        /// получает объект
        /// </summary>
        /// <returns></returns>

        public async Task<Client> Get(int id)
        {
            try
            {
                var client =  await _repository.Get(e => e.Id == id).FirstOrDefaultAsync();
                return client;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }

        /// <summary>
        /// обновляет объект
        /// </summary>
        /// <returns></returns>
        public async Task Update(Client client)
        {
            try
            {
                _repository.Update(client);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        /// <summary>
        /// удалает объект
        /// </summary>
        /// <returns></returns>

        public async Task Delete(long id)
        {
            try
            {
                var client = await _repository.Get(e => e.Id == id).FirstOrDefaultAsync();
                if (client == null)
                {
                    _logger.LogInformation($"Клиента под Id {id}, которого отправили на удаление, не обнаружено");
                    return;
                }
                
                _repository.Delete(client);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        
        /// <summary>
        /// В основном в качестве аргументов принимаем интерфейсы, чтобы быть более гибкими,
        /// лист в этом случае был слишком конкретным
        /// </summary>
        /// <param name="clients"></param>
        /// <returns></returns>
        public async Task CreateMany(IEnumerable<Client> clients)
        {
            try
            {
                await _repository.CreateManyAsync(clients);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
    }
}
