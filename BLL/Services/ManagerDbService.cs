﻿using BLL.Abstractions;
using DLL.Abstractions;
using DLL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ManagerDbService : IDbService<Manager>
    {
        private readonly IDbRepository<Manager> _repository;
        private ILogger<ManagerDbService> _logger;

        public ManagerDbService(IDbRepository<Manager> dbRepository, ILogger<ManagerDbService> logger)
        {
            _logger = logger;
            _repository = dbRepository;
        }
        public async Task Create(Manager manager)
        {
            try
            {
                await _repository.CreateAsync(manager);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }

        public async Task<Manager> Get(Func<Manager, bool> predicate)
        {
            try
            {
                var managers = _repository.Get(predicate);
                var manager = await managers.ToList().FirstOrDefaultAsync();
                if (manager == null)
                    _logger.LogCritical("Объект Manager не найден");
                return manager;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }
        public async Task<List<Manager>> GetMany(Func<Manager, bool> predicate)
        {
            try
            {
                var managers = _repository.Get(predicate);
                var managerList = managers.ToList();
                return managerList;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }


        /// <summary>
        /// получает объект
        /// </summary>
        /// <returns></returns>

        public async Task<Manager> Get(int id)
        {
            try
            {
                var managers = _repository.Get(e => e.Id == id).ToList();
                var manager = managers.FirstOrDefault();
                if (manager == null)
                    _logger.LogCritical("Объект Manager не найден");
                return manager;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }



        /// <summary>
        /// обновляет объект
        /// </summary>
        /// <returns></returns>

        public async Task Update(Manager manager)
        {
            try
            {
                _repository.Update(manager);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        /// <summary>
        /// удалает объект
        /// </summary>
        /// <returns></returns>

        public async Task Delete(long id)
        {
            try
            {
                var managers = _repository.Get(e => e.Id == id);
                _repository.Delete(managers.FirstOrDefault());
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        public async Task CreateMany(IEnumerable<Manager> manager)
        {
            try
            {
                await _repository.CreateManyAsync(manager);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
    }
}
