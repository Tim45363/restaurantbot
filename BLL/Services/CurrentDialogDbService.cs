﻿using BLL.Abstractions;
using DLL.Abstractions;
using DLL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CurrentDialogDbService : IDbService<CurrentDialog>
    {
        private readonly IDbRepository<CurrentDialog> _repository;
        private ILogger<CurrentDialogDbService> _logger;
        public CurrentDialogDbService(IDbRepository<CurrentDialog> dbRepository, ILogger<CurrentDialogDbService> logger)
        {
            _logger = logger;
            _repository = dbRepository;
        }
        public async Task Create(CurrentDialog dialog)
        {
            try
            {
                await _repository.CreateAsync(dialog);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }

        public async Task<CurrentDialog> Get(Func<CurrentDialog, bool> predicate)
        {
            try
            {
                var dialog = await _repository.Get(predicate).Include(d => d.Client)
                    .FirstOrDefaultAsync(); 
                    
                if (dialog == null)
                    _logger.LogCritical("Объект CurrentDialog не найден");
                
                return dialog;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }
        public async Task<List<CurrentDialog>> GetMany(Func<CurrentDialog, bool> predicate)
        {
            try
            {
                var dialogs = await _repository.Get(predicate)
                    .Include(d => d.Client)
                    .ToListAsync();
                return dialogs;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }


        /// <summary>
        /// получает объект
        /// </summary>
        /// <returns></returns>

        public async Task<CurrentDialog> Get(int id)
        {
            try
            {
                var dialog = await _repository.Get(e => e.Id == id)
                    .Include(d => d.Client)
                    .FirstOrDefaultAsync();
                if (dialog == null)
                    _logger.LogInformation($"Объект CurrentDialog под ID {id} не найден");
                return dialog;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }



        /// <summary>
        /// обновляет объект
        /// </summary>
        /// <returns></returns>

        public async Task Update(CurrentDialog dialog)
        {
            try
            {
                _repository.Update(dialog);
                await _repository.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
            }
        }
        /// <summary>
        /// удалает объект
        /// </summary>
        /// <returns></returns>

        public async Task Delete(long id)
        {
            try
            {
                var dialog =  _repository
                    .Get(e => e.Id == id)
                    .FirstOrDefault();

                if (dialog == null)
                {
                    _logger.LogCritical($"Не удалось найти диалог под Id {id}, отправленный на удаление");
                    return;
                }
                
                _repository.Delete(dialog);
                await _repository.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
            }
        }
        
        public async Task CreateMany(IEnumerable<CurrentDialog> dialogs)
        {
            try
            {
                await _repository.CreateManyAsync(dialogs);
                await _repository.SaveChangesAsync();
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
            }
        }
    }
}
