﻿using BLL.Abstractions;
using DLL.Abstractions;
using DLL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class CountryDbService : IDbService<Country>
    {
        private readonly IDbRepository<Country> _repository;
        private ILogger<CountryDbService> _logger;
        public CountryDbService(IDbRepository<Country> dbRepository, ILogger<CountryDbService> logger)
        {
            _logger = logger;
            _repository = dbRepository;
        }
        public async Task Create(Country country)
        {
            try
            {
                await _repository.CreateAsync(country);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }

        public async Task<Country> Get(Func<Country, bool> predicate)
        {
            try
            {
                var countries = _repository.Get(predicate);
                var country = countries.ToList().FirstOrDefault();
                if (country == null)
                    return null;
                return country;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }
        public async Task<List<Country>> GetMany(Func<Country, bool> predicate)
        {
            try
            {
                var countries = _repository.Get(predicate);
                var countriesList = countries.ToList();
                return countriesList;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }


        /// <summary>
        /// получает объект
        /// </summary>
        /// <returns></returns>

        public async Task<Country> Get(int id)
        {
            try
            {
                var countries = _repository.Get(e => e.Id == id).ToList();
                var country = countries.FirstOrDefault();
                if (country == null)
                    _logger.LogCritical("Объект не найден");
                return country;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message);
                return null;
            }
        }



        /// <summary>
        /// обновляет объект
        /// </summary>
        /// <returns></returns>

        public async Task Update(Country country)
        {
            try
            {
                _repository.Update(country);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        /// <summary>
        /// удалает объект
        /// </summary>
        /// <returns></returns>

        public async Task Delete(long id)
        {
            try
            {
                var countries = _repository.Get(e => e.Id == id);
                _repository.Delete(countries.FirstOrDefault());
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        public async Task CreateMany(IEnumerable<Country> countries)
        {
            try
            {
                await _repository.CreateManyAsync(countries);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
    }
}
