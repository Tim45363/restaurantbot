﻿using BLL.Abstractions;
using DLL.Abstractions;
using DLL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class RestaurantDbService : IDbService<Restaurant>
    {
        private readonly IDbRepository<Restaurant> _repository;
        private ILogger<RestaurantDbService> _logger;

        public RestaurantDbService() { }
        public RestaurantDbService(IDbRepository<Restaurant> dbRepository, ILogger<RestaurantDbService> logger)
        {
            _logger = logger;
            _repository = dbRepository;
        }
        public async Task Create(Restaurant regularQuestion)
        {
            try
            {
                await _repository.CreateAsync(regularQuestion);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }

        public async Task<Restaurant> Get(Func<Restaurant, bool> predicate)
        {
            try
            {
                var restaurants = _repository.Get(predicate)
                    .Include(r => r.Tables)
                    .Include(r => r.Reservations)
                    .Include(r => r.City);


                var restaurant = await restaurants.FirstOrDefaultAsync();

                if (restaurant == null)
                    _logger.LogCritical("Объект Restaurant не найден");
                return restaurant;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }
        public async Task<List<Restaurant>> GetMany(Func<Restaurant, bool> predicate)
        {
            try
            {
                var restaurants = await _repository.Get(predicate)
                    .Include(r => r.City)
                    .Include(r => r.Managers)
                    .Include(r => r.Reservations)
                    .Include(r => r.Tables)
                    .ToListAsync();

                return restaurants; 
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }


        /// <summary>
        /// получает объект
        /// </summary>
        /// <returns></returns>

        public async Task<Restaurant> Get(int id)
        {
            try
            {
                var regularQuestions = _repository.Get(e => e.Id == id).ToList();
                var regularQuestion = regularQuestions.FirstOrDefault();
                if (regularQuestion == null)
                    _logger.LogCritical("Объект Restaurant не найден");
                return regularQuestion;
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
                return null;
            }
        }



        /// <summary>
        /// обновляет объект
        /// </summary>
        /// <returns></returns>

        public async Task Update(Restaurant manager)
        {
            try
            {
                _repository.Update(manager);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        /// <summary>
        /// удалает объект
        /// </summary>
        /// <returns></returns>

        public async Task Delete(long id)
        {
            try
            {
                var managers = _repository.Get(e => e.Id == id);
                _repository.Delete(managers.FirstOrDefault());
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
        public async Task CreateMany(IEnumerable<Restaurant> manager)
        {
            try
            {
                await _repository.CreateManyAsync(manager);
                await _repository.SaveChangesAsync();
            }
            catch (Exception exception)
            {
                _logger.LogCritical(exception.Message);
            }
        }
    }
}
