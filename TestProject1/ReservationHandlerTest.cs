using BLL.Abstractions;
using BLL.BotMessageSender.Abstractions;
using BLL.ReservationManager;
using BLL.ReservationManager.Models;
using BLL.Services;
using DLL.Entities;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework.Internal;
using System;
using System.Collections.Generic;
using Xunit;

namespace RestaurantTest
{
    public class ReservationHandlerTest
    {
        //private IDbService<Restaurant> _restaurantService;
        private Mock<ILogger<ReservationHandler>> _logger  = new Mock<ILogger<ReservationHandler>>();
        private Mock<IDbService<Table>> tableService = new Mock<IDbService<Table>>();
        private Mock<IDbService<Reservation>> _reservationService = new Mock<IDbService<Reservation>>();
        private Mock<IBotMessageSender> _botMessageSender = new Mock<IBotMessageSender>();
        private Mock<IDbService<Client>> _clientService = new Mock<IDbService<Client>>();

        [Fact]
        public void IsDateTimeFreeTest()
        {
            var restaurant = GetMockRestaurant();

            var restaurantServiceMock = 
                Mock.Of<IDbService<Restaurant>>(rest => rest.Get(It.IsAny<Func<Restaurant, bool>>()).Result
            == new Restaurant
            {
                Name = restaurant.Name,
                Reservations = restaurant.Reservations,
                Tables = restaurant.Tables
            });

            var reservationHandler = new ReservationHandler(restaurantServiceMock, _logger.Object, tableService.Object,
                _reservationService.Object, _botMessageSender.Object, _clientService.Object);

            var reservationModel = new ReservationModel 
            { ReservationTime = DateTime.Now.AddHours(7).AddMinutes(15),
            RestaurantName = restaurant.Name };
            var result = reservationHandler.IsDateTimeFree(reservationModel).Result;

            Assert.True(result);
        }

        //[Fact]
        //public void GetFreeTimeScheduleByDayTest()
        //{
            
        //    var restaurant = GetMockRestaurant();
        //    var restaurantServiceMock =
        //    Mock.Of<IDbService<Restaurant>>(rest => rest.Get(It.IsAny<Func<Restaurant, bool>>()).Result
        //        == new Restaurant
        //    {
        //        Name = restaurant.Name,
        //        Reservations = restaurant.Reservations,
        //        Tables = restaurant.Tables
        //    });

        //    var reservationHandler = new ReservationHandler(restaurantServiceMock, _logger.Object, tableService.Object,
        //        _reservationService.Object, _botMessageSender.Object, _clientService.Object);

        //    var nowData = DateTime.Now.Date;
        //    var freeTimeScheduleByDay = reservationHandler.GetFreeTimeScheduleByDay(restaurant.Name, nowData).Result;

        //    Assert.True(freeTimeScheduleByDay.Count != 0);
        //}

        private Restaurant GetMockRestaurant()
        {
            var restaurantName = "NY";
            var tables = new List<Table> { new Table { Number = 1 } };
            var reservations = new List<Reservation> {
                new Reservation {  DurationInMinutes = 60,
                    Time = DateTime.Now.AddHours(1).ToLocalTime(), Table = new Table{ Number = 1} },
                new Reservation {  DurationInMinutes = 60,
                    Time = DateTime.Now.AddHours(1).ToLocalTime(), Table = new Table{ Number = 1} }};
            var restaurant = new Restaurant
            {
                Name = restaurantName,
                Reservations = reservations,
                Tables = tables
            };
            restaurant.Reservations.ForEach(r => r.Restaurant = restaurant);
            return restaurant;
        }
    }
}
