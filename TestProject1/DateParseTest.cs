﻿using BLL.BotDialogProcessing.DialogAlgorithms.ReservationDialogTemplate_2.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace RestaurantTest
{
    public class DateParseTest
    {

        [Fact]
        public void ParseMessageIntoDateTest()
        {
            try
            {
                var t1 = ParseMessageIntoDate.TryParseDateToString("1 января");
                var t2 = ParseMessageIntoDate.TryParseDateToString("5 января");
                var t3 = ParseMessageIntoDate.TryParseDateToString("32 января");

                Assert.True((t1 != null) && (t2 != null) && (t3 == null));
            }
            catch
            {
                Assert.True(false);
            }


        }
    }
}
